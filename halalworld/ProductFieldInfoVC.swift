//
//  ProductFieldInfoVC.swift
//  HalalWorld
//
//  Created by Crocodic Studio on 4/26/16.
//  Copyright © 2016 Crocodic Studio. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ProductFieldInfoVC: UITableViewController {

    var categories = [Category]()
    
    @IBOutlet weak var nameProduct: TextFieldInset!
    @IBOutlet weak var categoryProduct: UIButton!
    @IBOutlet weak var categoryIndicator: UIActivityIndicatorView!
    @IBOutlet weak var priceProduct: TextFieldInset!
    @IBOutlet weak var descriptionProduct: TextFieldInset!
    
    var categoryId = Int()
    var categorySelected = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        categoryProduct.addTarget(self, action: #selector(PlaceFieldInfoVC.ShowAlert(sender:)), for: .touchUpInside)
        self.postCategory()
    }
    
    func Reset() {
        self.nameProduct.text = ""
        self.categorySelected = ""
        self.categoryId = 0
        let colorDeselect = UIColor(red: 204/255, green: 204/255, blue: 204/255, alpha: 1.0)
        self.categoryProduct.setTitleColor(colorDeselect, for: .normal)
        self.categoryProduct.setTitle("Category", for: .normal)
        self.priceProduct.text = ""
        self.descriptionProduct.text = ""
    }
    
    func ShowAlert(sender: AnyObject) {
        self.alertSheet(categories: categories)
    }
    
    func alertSheet(categories: [Category]) {
        self.view.endEditing(true)
        if self.categorySelected != "" {
            self.categoryProduct.setTitleColor(UIColor.black, for: .normal)
        } else {
            let colorDeselect = UIColor(red: 204/255, green: 204/255, blue: 204/255, alpha: 1.0)
            self.categoryProduct.setTitleColor(colorDeselect, for: .normal)
        }
        
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        for i in 0 ..< categories.count {
            var sheet = UIAlertAction()
            if i == categories.count - 1 {
                sheet = UIAlertAction(title: categories[i].name, style: .cancel, handler: nil)
            } else {
                sheet = UIAlertAction(title: categories[i].name, style: .default) { (action) -> Void in
                    self.categoryProduct.setTitle(categories[i].name, for: .normal)
                    self.categoryProduct.setTitleColor(UIColor.black, for: .normal)
                    self.categoryId = self.categories[i].id
                    self.categorySelected = self.categories[i].name
                }
                sheet.setValue(0, forKey: "titleTextAlignment")
            }
            actionSheet.addAction(sheet)
        }
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func postCategory() {
        let headers = [
            "User-Agent": "iphone",
            "Referer": "CROCODICHEBAT"
        ]
        
        let url = URL(string: Config().BASE_URL + Config().PRODUCT_CATEGORY + Config().SecretKey())!
        Alamofire.request(url, method: .post, headers: headers)
            .responseJSON { (response) in
                if response.result.isSuccess {
                let json = try! JSON(data:response.data!)
                let status = json["api_status"].intValue
                    if status == 1 {
                        let dataTotal = json["api_total_data"].intValue
                        for i in 0 ..< dataTotal {
                            let id = json["data"][i]["id"].intValue
                            let name = json["data"][i]["name"].stringValue
                            self.categories.append(Category(id: id, name: name))
                        }
                        self.categories.append(Category(name: "Cancel"))
                    } else {
                        return self.postCategory()
                    }
                } else {
                    
                }
                self.categoryProduct.setTitle("Category", for: .normal)
                self.categoryIndicator.stopAnimating()
                self.categoryProduct.isEnabled = true
        }
    }}
