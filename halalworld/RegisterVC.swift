//
//  RegisterVC.swift
//  halalworld
//
//  Created by Crocodic Studio on 3/29/16.
//  Copyright © 2016 Crocodic Studio. All rights reserved.
//

import UIKit

class RegisterVC: UIViewController {

    @IBOutlet weak var container: UIView!
    @IBOutlet weak var activityInd: UIActivityIndicatorView!
    
    private var registerEmbed: RegisterTableVC!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        
        container.layer.cornerRadius = 5
        container.clipsToBounds = true
        
        registerEmbed.activityInd = self.activityInd
        
        let image = UIImage(named: "logo_halal_world_2.png")
        let height = CGFloat((self.navigationController?.navigationBar.frame.height)!)
        let rect = CGRect(x: 0, y: 0, width: 200, height: height)
        let titleImage = UIImageView(frame: rect)
        titleImage.image = image
        titleImage.contentMode = .scaleAspectFit
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "registerEmbed" {
            let vc = segue.destination as? RegisterTableVC
            self.registerEmbed = vc
        }
    }

}
