//
//  HomeVC.swift
//  halalworld
//
//  Created by Crocodic Studio on 3/22/16.
//  Copyright © 2016 Crocodic Studio. All rights reserved.
//

import UIKit
import GoogleMaps
import SwiftyJSON
import Alamofire
import Kingfisher
import CoreLocation

class HomeVC: UIViewController, UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate, TableViewCellDelegate {
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var productBtn: UIButton!
    @IBOutlet weak var placeBtn: UIButton!
    
    var leftBar = UIBarButtonItem()
    
    var items = [ListLocation]()
    private var datas = [ListPlaceLocation]()
    
    let locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: "TableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "cell")
        tableView.estimatedRowHeight = 30
        tableView.rowHeight = UITableView.automaticDimension
        
        let camera = GMSCameraPosition.camera(withLatitude: 36.204824, longitude: 138.252924, zoom: 4)
        self.mapView.camera = camera
        
        self.mapView.isMyLocationEnabled = true
        
        self.locationManager.delegate = self
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.startUpdatingLocation()
        
        self.items.append(ListLocation(name: "My Location Now"))
        self.items[0].isNearby = true
        
        postListLocation()
        
        checkList()
    }
        
    override func viewWillAppear(_ animated: Bool) {
    }
    
    func checkList() {
        let isNearby = UserDefaults.standard.bool(forKey: "isNearby")
        if !isNearby {
            self.postList()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let mylocation = mapView.myLocation {
            let myLat = mylocation.coordinate.latitude
            let myLong = mylocation.coordinate.longitude
            UserDefaults.standard.set(myLat, forKey: "myLocationLat")
            UserDefaults.standard.set(myLong, forKey: "myLocationLong")
            let camera = GMSCameraPosition.camera(withTarget: mylocation.coordinate, zoom: 9)
            mapView.camera = camera
            let isNearby = UserDefaults.standard.bool(forKey: "isNearby")
            if isNearby {
                self.nearbyPlace(lat: myLat, long: myLong)
            }
            print("User's location is know")
            self.locationManager.stopUpdatingLocation()
        } else {
            print("User's location is unknown")
        }
    }
    
    func postListLocation() {
        
        let headers = [
            "User-Agent": "iphone",
            "Referer": "CROCODICHEBAT"
        ]
        
        let url = URL(string: Config().BASE_URL + Config().LIST_LOCATION + Config().SecretKey())!
        Alamofire.request(url, method: .post, headers: headers)
            .responseJSON { (response) in
                if response.result.isSuccess {
                    let json = try! JSON(data:response.data!)
                    let status = json["api_status"].intValue
                    let totalData = json["api_total_data"].intValue
                    if status == 1 {
                        for i in 0 ..< totalData + 1 {
                            if i == totalData{
                                self.items.append(ListLocation(name: "Cancel"))
                            } else {
                                let id = json["data"][i]["id"].intValue
                                let name = json["data"][i]["name"].stringValue
                                self.items.append(ListLocation(id: id, name: name))
                            }
                        }
                    } else {
                        return self.postListLocation()
                    }
                } else {
                    return self.postListLocation()
                }
        }
    }
    
    @objc func selectLocation(sender: AnyObject) {
        if items.count <= 0 {
            return
        }
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let imageView = UIImageView(frame: CGRect(x: actionSheet.view.bounds.size.width * 0.8, y: 10, width: 40, height: 40))
        let image = UIImage(named: "location_icon_44.png")
        imageView.image = image
        actionSheet.view.addSubview(imageView)
        
        for i in 0 ..< items.count {
            var sheet = UIAlertAction()
            if i == items.count - 1 {
                sheet = UIAlertAction(title: items[i].name, style: .cancel, handler: nil)
            } else {
                sheet = UIAlertAction(title: items[i].name, style: .default) { (action) -> Void in
                    UserDefaults.standard.set(self.items[i].isNearby, forKey: "isNearby")
                    UserDefaults.standard.set(self.items[i].id, forKey: "id_location")
                    self.locationManager.startUpdatingLocation()
                    self.checkList()
                }
                sheet.setValue(0, forKey: "titleTextAlignment")
            }
            actionSheet.addAction(sheet)
        }
        
        let rectTitleView = CGRect(x: 0, y: -50, width: actionSheet.view.bounds.size.width - 21, height: 60 - 5.2)
        let customTitleView = UIView(frame: rectTitleView)
        let rectTitle = CGRect(x: 0, y: 0, width: actionSheet.view.bounds.size.width - 21, height: 60 - 5.2)
        let newTitle = UILabel(frame: rectTitle)
        newTitle.text = "Choose location"
        newTitle.textAlignment = .center
        newTitle.textColor = UIColor.white
        newTitle.font = UIFont(name: "Raleway", size: 20)
        
        customTitleView.addSubview(newTitle)
        actionSheet.view.addSubview(customTitleView)
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func nearbyPlace(lat: Double, long: Double) {
        let parameters = [
            "lat": lat,
            "long": long
        ]
        
        let headers = [
            "User-Agent": "iphone",
            "Referer": "CROCODICHEBAT"
        ]
        let url = URL(string: Config().BASE_API_CUSTOM + Config().LIST_NEARBY_PLACE + Config().SecretKey())!
        Alamofire.request(url, method: .post, parameters: parameters, headers: headers)
            .responseJSON { (response) in
                if response.result.isSuccess {
                let json = try! JSON(data:response.data!)
                let status = json["api_status"].intValue
                if status == 1 {
                    self.datas.removeAll()
                        self.mapView.clear()
                        let dataTotal = json["api_total_data"].intValue
                        for i in 0 ..< dataTotal {
                            let id = json["data"][i]["id"].intValue
                            let favoriteId = json["data"][i]["id_favorite"].intValue
                            let photo = json [ "data"][i]["url_photo"].stringValue
                            let name = json["data"][i]["name"].stringValue
                            let description = json["data"][i]["description"].stringValue
                            let placeCategory = json["data"][i]["category_name"].stringValue
                            let lat = json["data"][i]["lat"].doubleValue
                            let long = json["data"][i]["long"].doubleValue
                            let distance = json["data"][i]["distance_in_km"].stringValue
                            let favorite = json["data"][i]["is_favorite"].intValue != 0 ? true : false
                            let appstore = json["data"][i]["appstore"].stringValue
                            self.setMarker(name: name, latitude: lat, longitude: long)
                            self.datas.append(ListPlaceLocation(id: id, favoriteId: favoriteId, photo: photo, name: name, description: description, placeCategory: placeCategory, lat: lat, long: long, isFavorite: favorite, appstore: appstore))
                            self.datas[i].direction = distance
                        }
                    } else {
                        self.tableView.separatorStyle = .none
                    }
                } else {
                    
                }
                self.doRefresh()
        }
    }
    
    func postList() {
        let id_member = UserDefaults.standard.integer(forKey:"userId")
        let id_location = UserDefaults.standard.integer(forKey:"id_location")
        
        let parameters = [
            "id_member": id_member,
            "id_location": id_location
        ]
            
        let headers = [
            "User-Agent": "iphone",
            "Referer": "CROCODICHEBAT"
        ]
        
        let url = URL(string: Config().BASE_URL + Config().LIST_PLACE_BY_LOCATION + Config().SecretKey())!
        
        Alamofire.request(url, method: .post, parameters: parameters, headers: headers)
            .responseJSON { (response) in
                if response.result.isSuccess {
                    let json = try! JSON(data:response.data!)
                    let status = json["api_status"].intValue
                    if status == 1 {
                        self.datas.removeAll()
                        self.mapView.clear()
                        let dataTotal = json["api_total_data"].intValue
                        for i in 0 ..< dataTotal {
                            let id = json["data"][i]["id"].intValue
                            let favoriteId = json["data"][i]["id_favorite"].intValue
                            let photo = json["data"][i]["url_photo"].stringValue
                            let name = json["data"][i]["name"].stringValue
                            let description = json["data"][i]["description"].stringValue
                            let placeCategory = json["data"][i]["placecategory_name"].stringValue
                            let lat = json["data"][i]["lat"].doubleValue
                            let long = json["data"][i]["long"].doubleValue
                            let favorite = json["data"][i]["is_favorite"].intValue != 0 ? true : false
                            let appstore = json["data"][i]["appstore"].stringValue
                            self.setMarker(name: name, latitude: lat, longitude: long)
                            self.datas.append(
                                ListPlaceLocation(
                                    id: id,
                                    favoriteId: favoriteId,
                                    photo: photo,
                                    name: name,
                                    description: description,
                                    placeCategory: placeCategory,
                                    lat: lat,
                                    long: long,
                                    isFavorite: favorite,
                                    appstore: appstore))
                        }
                    } else {
                        self.datas.removeAll()
                        self.mapView.clear()
                        self.tableView.separatorStyle = .none
                    }
                } else {
                    return self.postList()
                }
                self.doRefresh()
        }
    }
    
    func doRefresh(){
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func setMarker(name: String, latitude: Double, longitude: Double) {
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2DMake(latitude, longitude)
        marker.snippet = name
        marker.icon = UIImage(named: "pin_map.png")
        marker.appearAnimation = GMSMarkerAnimation.pop
        marker.map = self.mapView
    }
    
    func shareBtn(cell: TableViewCell) {
        let indexPath = tableView.indexPathForRow(at: cell.center)!
        let placeName = self.datas[indexPath.row].name
        let placeDescription = self.datas[indexPath.row].description.isEmpty ? "-" : self.datas[indexPath.row].description.trunc(length: 30)
        let shareUrl = self.datas[indexPath.row].appstore
        let photo = self.datas[indexPath.row].photo
        
        let url = URL(string: photo)!
        ImageCache.default.retrieveImage(forKey: photo, options: nil, completionHandler: { (image, cacheType) in
            if let image = image {
                let objectsToShare = [image, placeName, placeDescription, shareUrl] as [Any]
                let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                self.present(activityVC, animated: true, completion: nil)
            } else {
                ImageDownloader.default.downloadImage(with: url, progressBlock: nil, completionHandler: { (image, error, imageURL, originalData) in
                    if error == nil {
                        ImageCache.default.store(image!, forKey: photo)
                        let objectsToShare = [image!, placeName, placeDescription, shareUrl] as [Any]
                        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                        self.present(activityVC, animated: true, completion: nil)
                    }
                })
            }
        })
    }
    
    func favoriteBtn(cell: TableViewCell) {
        let indexPath = tableView.indexPathForRow(at: cell.center)!
        let isUserLoggedIn = UserDefaults.standard.bool(forKey: "isUserLoggedIn")
        if !isUserLoggedIn {
            return
        }
        if datas[indexPath.row].isFavorite {
            removeFavorite(cell: cell, indexPath: indexPath)
        } else {
            addFavorite(cell: cell, indexPath: indexPath)
        }
    }
    
    func removeFavorite(cell: TableViewCell, indexPath: IndexPath) {
        cell.favoriteBtn.isEnabled = false
        let id = datas[indexPath.row].favoriteId
        let headers = [
            "User-Agent": "iphone",
            "Referer": "CROCODICHEBAT"
        ]
        
        let url = URL(string: Config().BASE_URL + Config().DELETE_FAVORITE + Config().SecretKey())!
        Alamofire.request(url, method: .post, parameters: ["id": id], headers: headers)
            .responseJSON { (response) in
                if response.result.isSuccess {
                    let json = try! JSON(data:response.data!)
                    let status = json["api_status"].intValue
                    if status == 1 {
                        self.datas[indexPath.row].isFavorite = false
                        cell.favoriteBtn.setImage(cell.favoriteOff, for: .normal)
                        let isNearby = UserDefaults.standard.bool(forKey: "isNearby")
                        if isNearby {
                            let myLat = UserDefaults.standard.double(forKey: "myLocationLat")
                            let myLong = UserDefaults.standard.double(forKey: "myLocationLong")
                            self.nearbyPlace(lat: myLat, long: myLong)
                        } else {
                            self.postList()
                        }
                    } else {
                        
                    }
                } else {
                    
                }
                cell.favoriteBtn.isEnabled = true
        }
    }
    
    func addFavorite(cell: TableViewCell, indexPath: IndexPath) {
        cell.favoriteBtn.isEnabled = false
        let id = datas[indexPath.row].id
        let idMember = UserDefaults.standard.integer(forKey:"userId")
        
        let parameters = [
            "id_member": idMember,
            "placepro": "place",
            "id_place": id
            ] as [String : Any]
        let headers = [
            "User-Agent": "iphone",
            "Referer": "CROCODICHEBAT"
        ]
        
        let url = URL(string: Config().BASE_URL + Config().ADD_FAVORITE_PLACE + Config().SecretKey())!
        Alamofire.request(url, method: .post, parameters: parameters, headers: headers)
            .responseJSON { (response) in
                if response.result.isSuccess {
                    let json = try! JSON(data:response.data!)
                    let status = json["api_status"].intValue
                    if status == 1 {
                        self.datas[indexPath.row].isFavorite = true
                        cell.favoriteBtn.setImage(cell.favoriteOn, for: .normal)
                        
                    } else {
                        
                    }
                } else {
                    
                }
                cell.favoriteBtn.isEnabled = true
                
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "cell") as! TableViewCell
        cell.delegate = self
        let photo = datas[indexPath.row].photo
        let name = datas[indexPath.row].name
        let category = datas[indexPath.row].placeCategory
        let direction = datas[indexPath.row].direction
        let favorite = datas[indexPath.row].isFavorite
        let lat = datas[indexPath.row].lat
        let long = datas[indexPath.row].long
        let coordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
        
        let url = URL(string: photo)
        cell.photoImage.kf.setImage(with: url) { (image, error, cacheType, imageURL) in
            cell.photoImage.image = cell.photoImage.image?.circle
            cell.activityId.stopAnimating()
        }
        
        cell.configureData(name: name, category: category, distance: direction, favorite: favorite)
        cell.getDirection(targetLocation: coordinate)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let placeId = datas[indexPath.row].id
        UserDefaults.standard.set(placeId, forKey: "placeId")
        
        let mainStoryBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc:UIViewController = mainStoryBoard.instantiateViewController(withIdentifier: "DetailPlace")
        self.present(vc, animated: true, completion: nil)
    }
}
