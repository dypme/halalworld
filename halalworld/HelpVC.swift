//
//  HelpVC.swift
//  HalalWorld
//
//  Created by Crocodic Studio on 5/23/16.
//  Copyright © 2016 Crocodic Studio. All rights reserved.
//

import UIKit

class HelpVC: UIViewController {

    @IBOutlet weak var helpTitle: UILabel!
    @IBOutlet weak var helpContent: UILabel!
    
    var helpTitleString = String()
    var helpContentString = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        helpTitle.text = helpTitleString
        helpContent.text = helpContentString
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
