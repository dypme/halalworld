//
//  RaiseTabBar.swift
//  halalworld
//
//  Created by Crocodic Studio on 3/30/16.
//  Copyright © 2016 Crocodic Studio. All rights reserved.
//

import UIKit

class RaiseTabBarController: UITabBarController {
    
    @IBOutlet weak var centerButton: UIButton!
    @IBOutlet weak var plusController: UIViewController!
    
    var controller = SuggestVC()
    
    let items = ["Product", "Place", "Cancel"]
    
    override func viewDidLoad() {
        
        let navController = self.viewControllers![2] as! UINavigationController
        controller = navController.viewControllers[0] as! SuggestVC
        
        tabBar.layer.borderWidth = 0
        tabBar.layer.borderColor = UIColor.clear.cgColor
        
        self.addCenterButtonWithImage(buttonImage: UIImage(named: "suggest_menu_icon2.png")!,
                                      highlightImage: UIImage(named: "suggest_menu_icon2.png")!,
                                      target: self,
                                      action: #selector(buttonPressed(sender:)))
        
    }
    
    func addCenterButtonWithImage(buttonImage: UIImage, highlightImage: UIImage, target: AnyObject, action: Selector) {
        let button: UIButton = UIButton(type: .custom)
        button.autoresizingMask = [.flexibleRightMargin, .flexibleLeftMargin, .flexibleBottomMargin, .flexibleTopMargin]
        
        button.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        
        button.setBackgroundImage(buttonImage, for: .normal)
        button.setBackgroundImage(highlightImage, for: .highlighted)
        
        let heightDifference:CGFloat = buttonImage.size.height - self.tabBar.frame.size.height
        
        if heightDifference < 0 {
            button.center = self.tabBar.center
            
        } else {
            var center:CGPoint = self.tabBar.center
            center.y = center.y - heightDifference/1.5
            button.center = center
        }
        
        
        button.addTarget(target, action: action, for: .touchUpInside)
        self.view.addSubview(button)
        
        self.centerButton = button
    }
    
    func resetCenterButton() {
        let buttonImage = UIImage(named: "suggest_menu_icon2.png")!
        let heightDifference:CGFloat = buttonImage.size.height - self.tabBar.frame.size.height
        
        if heightDifference < 0 {
            self.centerButton.center = self.tabBar.center
            
        } else {
            var center:CGPoint = self.tabBar.center
            center.y = heightDifference/1.5
            self.centerButton.center = center
        }
        
        self.tabBar.addSubview(centerButton)
    }
    
    @objc func buttonPressed(sender: AnyObject) {
        self.selectSuggestSheet()
        self.perform(#selector(RaiseTabBarController.doHighlight(b:)), with: sender, afterDelay: 0)
    }
    
    @objc func doHighlight(b: UIButton) {
        b.isHighlighted = true
    }
    
    @objc func doNotHighlight(b: UIButton) {
        b.isHighlighted = false
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if item.tag != 1 {
            self.perform(#selector(RaiseTabBarController.doNotHighlight(b:)), with: centerButton, afterDelay: 0)
            UserDefaults.standard.removeObject(forKey: "suggestIndex")
        } else {
            self.selectSuggestSheet()
            self.perform(#selector(RaiseTabBarController.doHighlight(b:)), with: centerButton, afterDelay: 0)
        }
    }
    
    
    func selectSuggestSheet() {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        for i in 0 ..< items.count {
            var sheet = UIAlertAction()
            if i == items.count - 1 {
                sheet = UIAlertAction(title: items[i], style: .cancel, handler: nil)
            } else {
                sheet = UIAlertAction(title: items[i], style: .default) { (action) -> Void in
                    UserDefaults.standard.set(i, forKey: "suggestIndex")
                    if self.selectedIndex ==  2 {
                        self.controller.showSuggest(suggestIndex: i)
                    }
                    self.selectedIndex = 2
                }
                sheet.setValue(0, forKey: "titleTextAlignment")
            }
            actionSheet.addAction(sheet)
        }
        
        let rectTitleView = CGRect(x: 0, y: -50, width: actionSheet.view.bounds.size.width - 21, height: 60 - 5.2)
        let customTitleView = UIView(frame: rectTitleView)
        let rectTitle = CGRect(x: 0, y: 0, width: actionSheet.view.bounds.size.width - 21, height: 60 - 5.2)
        let newTitle = UILabel(frame: rectTitle)
        newTitle.text = "Choose suggest"
        newTitle.textAlignment = .center
        newTitle.textColor = UIColor.white
        newTitle.font = UIFont(name: "Raleway", size: 20)
        
        customTitleView.addSubview(newTitle)
        actionSheet.view.addSubview(customTitleView)
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
}
