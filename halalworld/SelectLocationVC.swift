//
//  SelectLoactionViewController.swift
//  halalworld
//
//  Created by Crocodic Studio on 3/18/16.
//  Copyright © 2016 Crocodic Studio. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class SelectLocationVC: UIViewController, UIActionSheetDelegate {

    @IBOutlet weak var selectBtn: UIButton!
    
    var items = [ListLocation]()
    
    @IBOutlet weak var activityId: UIActivityIndicatorView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        selectBtn.layer.cornerRadius = 20
        selectBtn.layer.borderWidth = 1
        
        selectBtn.addTarget(self, action: #selector(SelectLocationVC.selectLocation(sender:)), for: .touchUpInside)
        
        self.setNeedsStatusBarAppearanceUpdate()
        
        self.items.append(ListLocation(name: "My Location Now"))
        self.items[0].isNearby = true
        
        self.postListLocation()
        
        self.selectBtn.isEnabled = false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
        
    @objc func selectLocation(sender: AnyObject) {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let imageView = UIImageView(frame: CGRect(x: actionSheet.view.bounds.size.width * 0.8, y: 10, width: 40, height: 40))
        let image = UIImage(named: "location_icon_44.png")
        imageView.image = image
        actionSheet.view.addSubview(imageView)
        
        for i in 0 ..< items.count {
            var sheet = UIAlertAction()
            if i == items.count - 1 {
                sheet = UIAlertAction(title: items[i].name, style: .cancel, handler: nil)
            } else {
                sheet = UIAlertAction(title: items[i].name, style: .default) { (action) -> Void in
                    UserDefaults.standard.set(self.items[i].isNearby, forKey: "isNearby")
                    UserDefaults.standard.set(self.items[i].id, forKey: "id_location")
                    self.selectBtn.setTitle(self.items[i].name, for: .normal)
                    self.home(sender: self)
                    self.selectBtn.isEnabled = false
                }
                sheet.setValue(0, forKey: "titleTextAlignment")
            }
            actionSheet.addAction(sheet)
        }
        
        let rectTitleView = CGRect(x: 0, y: -50, width: actionSheet.view.bounds.size.width - 21, height: 60 - 5.2)
        let customTitleView = UIView(frame: rectTitleView)
        let rectTitle = CGRect(x: 0, y: 0, width: actionSheet.view.bounds.size.width - 21, height: 60 - 5.2)
        let newTitle = UILabel(frame: rectTitle)
        newTitle.text = "Choose location"
        newTitle.textAlignment = .center
        newTitle.textColor = UIColor.white
        newTitle.font = UIFont(name: "Raleway", size: 20)
        
        customTitleView.addSubview(newTitle)
        actionSheet.view.addSubview(customTitleView)
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func home(sender: AnyObject) {
        let mainStoryBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryBoard.instantiateViewController(withIdentifier: "Home")
        self.present(vc, animated: true, completion: nil)
    }
    
    func postListLocation() {
        let headers = [
            "User-Agent": "iphone",
            "Referer": "CROCODICHEBAT"
        ]
        
        let url = URL(string: Config().BASE_URL + Config().LIST_LOCATION + Config().SecretKey())!
        Alamofire.request(url, method: .post, headers: headers)
            .responseJSON { (response) in
                if response.result.isSuccess {
                    let json = try! JSON(data:response.data!)
                    let status = json["api_status"].intValue
                    let totalData = json["api_total_data"].intValue
                    if status == 1 {
                        for i in 0 ..< totalData + 1 {
                            if i == totalData{
                                self.items.append(ListLocation(name: "Cancel"))
                            } else {
                                let id = json["data"][i]["id"].intValue
                                let name = json["data"][i]["name"].stringValue
                                self.items.append(ListLocation(id: id, name: name))
                            }
                        }
                        self.selectBtn.isEnabled = true
                        self.selectBtn.setTitle("SELECT LOCATION", for: .normal)
                        self.activityId.stopAnimating()
                    } else {
                        return self.postListLocation()
                    }
                } else {
                    return self.postListLocation()
                }
        }
    }
}
