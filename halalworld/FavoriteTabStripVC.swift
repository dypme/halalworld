//
//  FavoriteTabStripViewController.swift
//  halalworld
//
//  Created by Crocodic Studio on 4/5/16.
//  Copyright © 2016 Crocodic Studio. All rights reserved.
//

import Foundation
import XLPagerTabStrip

class FavoriteTabStripVC: ButtonBarPagerTabStripViewController {
    
    let navBarColor = UIColor(red: 53/255, green: 106/255, blue: 160/255, alpha: 1)
    
    var tabProduct: FavoriteProductTableVC! = nil
    var tabPlace: FavoritePlaceTableVC! = nil
    
    var productDatas = [DataList]()
    var placeDatas = [DataList]()
    
    @IBOutlet weak var activityId: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        tabProduct = FavoriteProductTableVC(style: .plain, itemInfo: IndicatorInfo(title: "Product"), activityId: activityId)
        tabPlace = FavoritePlaceTableVC(style: .plain, itemInfo: IndicatorInfo(title: "Place"), activityId: activityId)
        
        // change selected bar color
        settings.style.buttonBarBackgroundColor = UIColor.black
        settings.style.buttonBarItemBackgroundColor = navBarColor
        settings.style.selectedBarBackgroundColor = UIColor.white
        settings.style.buttonBarItemFont = UIFont.systemFont(ofSize: 14)
        settings.style.selectedBarHeight = 3.0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = UIColor.white
        settings.style.buttonBarItemsShouldFillAvailiableWidth = true
        
        super.viewDidLoad()
        
        self.view.layoutIfNeeded()
        buttonBarView.selectedBar.frame.origin.y = buttonBarView.frame.size.height - settings.style.selectedBarHeight
        
    }
    
    
    
    // MARK: - PagerTabStripDataSource
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        return [tabProduct, tabPlace]
    }
    
}
