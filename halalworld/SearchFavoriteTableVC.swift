//
//  SearchFavoriteTableVC.swift
//  halalworld
//
//  Created by Crocodic Studio on 4/6/16.
//  Copyright © 2016 Crocodic Studio. All rights reserved.
//

import UIKit
import Kingfisher
import Alamofire
import SwiftyJSON

class SearchFavoriteTableVC: UITableViewController, TableViewCellDelegate {

    var productDatas = [DataList]()
    var placeDatas = [DataList]()
    var datas = [[DataList]]()
    var filtered = [[DataList]]()
    var tableTitle = [String]()
    var titleData = ["Product", "Place"]
    
    var searchController : UISearchController!
    
    var label = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: "TableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "cell")
        tableView.estimatedRowHeight = 30;
        tableView.rowHeight = UITableView.automaticDimension
        
        label.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
        label.text = "Data Not Found"
        label.textAlignment = .center
        label.isHidden = true
        view.addSubview(label)
        
    }
    
    override func viewDidLayoutSubviews() {
        label.center = self.view.center
    }
    
    func filterContentForSearchText(searchText: String, scope: String = "All") {
        
        var filteredProduct = [DataList]()
        var filteredPlace = [DataList]()
        
        if searchText.isEmpty {
            filteredProduct = productDatas
            filteredPlace = placeDatas
        } else {
            filteredProduct = productDatas.filter { filter in
                return filter.name.lowercased().contains(searchText.lowercased())
            }
            filteredPlace = placeDatas.filter { filter in
                return filter.name.lowercased().contains(searchText.lowercased())
            }
        }
        
        self.label.isHidden = true
        if filteredPlace.count > 0 && filteredProduct.count > 0 {
            filtered = [filteredProduct, filteredPlace]
            tableTitle = ["Product", "Place"]
        } else if filteredProduct.count > 0 {
            filtered = [filteredProduct]
            tableTitle = ["Product"]
        } else if filteredPlace.count > 0 {
            filtered = [filteredPlace]
            tableTitle = ["Place"]
        } else {
            filtered = []
            tableTitle = []
            self.label.isHidden = false
        }
        tableView.reloadData()
    }
    
    func shareBtn(cell: TableViewCell) {
        let indexPath = tableView.indexPathForRow(at: cell.center)!
        let placeName = self.filtered[indexPath.section][indexPath.row].name
        let placeDescription = self.filtered[indexPath.section][indexPath.row].description.isEmpty ? "-" : self.filtered[indexPath.section][indexPath.row].description.trunc(length: 30)
        let shareUrl = self.datas[indexPath.section][indexPath.row].appstore
        let photo = self.datas[indexPath.section][indexPath.row].photo
        
        let url = URL(string: photo)!
        ImageCache.default.retrieveImage(forKey: photo, options: nil, completionHandler: { (image, cacheType) in
            if let image = image {
                let objectsToShare = [image, placeName, placeDescription, shareUrl] as [Any]
                let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                self.present(activityVC, animated: true, completion: nil)
            } else {
                ImageDownloader.default.downloadImage(with: url, progressBlock: nil, completionHandler: { (image, error, imageURL, originalData) in
                    if error == nil {
                        ImageCache.default.store(image!, forKey: photo)
                        let objectsToShare = [image!, placeName, placeDescription, shareUrl] as [Any]
                        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                        self.present(activityVC, animated: true, completion: nil)
                    }
                })
            }
        })
    }
    
    func favoriteBtn(cell: TableViewCell) {
        let indexPath = tableView.indexPathForRow(at: cell.center)!
        let isUserLoggedIn = UserDefaults.standard.bool(forKey: "isUserLoggedIn")
        if !isUserLoggedIn {
            return
        }
        if filtered[indexPath.section][indexPath.row].favorite {
            removeFavorite(cell: cell, indexPath: indexPath)
        }
    }
    
    func removeFavorite(cell: TableViewCell, indexPath: IndexPath) {
        cell.favoriteBtn.isEnabled = false
        let id = filtered[indexPath.section][indexPath.row].favoriteId
        let headers = [
            "User-Agent": "iphone",
            "Referer": "CROCODICHEBAT"
        ]
        
        let url = URL(string: Config().BASE_URL + Config().DELETE_FAVORITE + Config().SecretKey())!
        Alamofire.request(url, method: .post, parameters: ["id": id], headers: headers)
            .responseJSON { (response) in
                if response.result.isSuccess {
                    let json = try! JSON(data:response.data!)
                    let status = json["api_status"].intValue
                    if status == 1 {
                        let filter = self.filtered[indexPath.section][indexPath.row].favoriteId
                        for i in 0 ..< self.productDatas.count {
                            if self.productDatas[i].favoriteId == filter {
                                self.productDatas.remove(at: i)
                                break
                            }
                        }
                        for i in 0 ..< self.placeDatas.count {
                            if self.placeDatas[i].favoriteId == filter {
                                self.placeDatas.remove(at: i)
                                break
                            }
                        }
                        for i in 0 ..< self.datas.count  {
                            for o in 0 ..< self.datas[i].count {
                                if self.datas[i][o].favoriteId == filter{
                                    self.datas[i].remove(at: o)
                                    break
                                }
                            }
                        }
                        
                        self.filtered[indexPath.section].remove(at: indexPath.row)
                        self.filterContentForSearchText(searchText: self.searchController.searchBar.text!)
                        if self.datas.count <= 0 {
                            self.label.isHidden = false
                        }
                    } else {
                        
                    }
                } else {
                    
                }
                cell.favoriteBtn.isEnabled = true
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        if searchController.isActive && !(searchController.searchBar.text?.isEmpty)!{
            return filtered.count
        }
        return filtered.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.isActive && !(searchController.searchBar.text?.isEmpty)! {
            return filtered[section].count
        }
        return filtered[section].count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! TableViewCell
        cell.delegate = self
        let filter: DataList
        
        if searchController.isActive && !(searchController.searchBar.text?.isEmpty)!{
            filter = filtered[indexPath.section][indexPath.row]
        } else {
            filter = filtered[indexPath.section][indexPath.row]
        }
        
        let photo = filter.photo
        let url = URL(string: photo)!
        cell.photoImage.kf.setImage(with: url) { (image, error, cacheType, imageURL) in
            cell.photoImage.image = cell.photoImage.image?.circle
            cell.activityId.stopAnimating()
        }
        
        cell.configureData(name: filter.name, category: filter.category, distance: filter.direction, favorite: true)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if searchController.isActive && !(searchController.searchBar.text?.isEmpty)! {
            return tableTitle[section]
        }
        return tableTitle[section]
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 45))
        let label = UILabel(frame: CGRect(x: 50, y: 0, width: tableView.frame.size.width, height: headerView.frame.size.height))
        let imageView = UIImageView(frame: CGRect(x: 17.5, y: 12.5, width: 20, height: 20))
        label.font = UIFont(name: "Raleway-Bold", size: 14)
        label.textColor = UIColor.white
        if searchController.isActive && (searchController.searchBar.text?.isEmpty)! {
            label.text = tableTitle[section]
        } else {
            if tableTitle.count > 0 {
                label.text = tableTitle[section]
            }
        }
        if label.text == "Product" {
            imageView.image = UIImage(named: "product_icon_44.png")
        } else {
            imageView.image = UIImage(named: "place_icon_44.png")
        }
        headerView.addSubview(label)
        headerView.addSubview(imageView)
        headerView.backgroundColor = UIColor(red: 63/255, green: 131/255, blue: 178/255, alpha: 1)
        
        return headerView
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 45.0
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let filter: DataList
        
        if searchController.isActive && !(searchController.searchBar.text?.isEmpty)!{
            filter = filtered[indexPath.section][indexPath.row]
        } else {
            filter = filtered[indexPath.section][indexPath.row]
        }
        let id = filter.id
        let favoriteId = filter.favoriteId
        let isFavorite = filter.favorite
        let mainStoryBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        var vc = UIViewController()
        if indexPath.section == 0 {
            UserDefaults.standard.set(favoriteId, forKey: "favoriteProductId")
            UserDefaults.standard.set(id, forKey: "productId")
            UserDefaults.standard.set(isFavorite, forKey: "isFavorite")
            vc = mainStoryBoard.instantiateViewController(withIdentifier: "DetailProduct")
        } else if indexPath.section == 1 {
            UserDefaults.standard.set(favoriteId, forKey: "favoritePlaceId")
            UserDefaults.standard.set(id, forKey: "placeId")
            UserDefaults.standard.set(isFavorite, forKey: "isFavorite")
            vc = mainStoryBoard.instantiateViewController(withIdentifier: "DetailPlace")
        }
        self.present(vc, animated: true, completion: nil)
    }
    
    func currentSellectedRowIndex() -> Int {
        let selectedIndexPath: IndexPath = self.tableView.indexPathForSelectedRow!
        return selectedIndexPath.row
    }
}
