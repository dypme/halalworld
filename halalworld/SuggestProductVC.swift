//
//  SuggestProductVC.swift
//  HalalWorld
//
//  Created by Crocodic Studio on 4/22/16.
//  Copyright © 2016 Crocodic Studio. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire
import SwiftyJSON
import CoreLocation

class SuggestProductVC: UIViewController, CLLocationManagerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var mapView: GMSMapView!
    let locationManager = CLLocationManager()
    @IBOutlet weak var activityId: UIActivityIndicatorView!
    
    @IBOutlet var photosBtn: [UIButton]!
    @IBOutlet var photosImage: [UIImageView]!
    var indexButton: Int!
    
    var imagePicker = UIImagePickerController()
    var isPhotos = [false, false, false, false, false]
    var photoName = ["photo product", "photo product 2", "barcode", "halal logo", "ingredients"]
    
    @IBOutlet weak var submitBtn: UIButton!
    
    let addPhoto = UIImage(named: "add_image_66.png")
    
    private var productInfo: ProductFieldInfoVC!
    private var productLocation: ProductFieldLocation!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.locationManager.delegate = self
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.startUpdatingLocation()
        
        let camera = GMSCameraPosition.camera(withLatitude: 36.204824, longitude: 138.252924, zoom: 4)
        self.mapView.camera = camera
        
        self.hideKeyboardWhenTappedAround()
        
        self.productLocation.locationProduct.addTarget(self, action: #selector(EndEditing(sender:)), for: .touchUpInside)
        
        submitBtn.backgroundColor = UIColor.clear
        submitBtn.layer.cornerRadius = 20
        submitBtn.layer.borderWidth = 1
        submitBtn.layer.borderColor = UIColor(red: 63/255, green: 131/255, blue: 178/255, alpha: 1).cgColor
        submitBtn.addTarget(self, action: #selector(SuggestProductVC.Submit(sender:)), for: .touchUpInside)
        
        for i in 0 ..< photosBtn.count {
            photosBtn[i].addTarget(self, action: #selector(setImage(sender:)), for: .touchUpInside)
        }
        
        mapView.isMyLocationEnabled = true
    }
    
    @objc func EndEditing(sender: AnyObject) {
        self.productInfo.view.endEditing(true)
        self.productLocation.ShowAlert()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let mylocation = mapView.myLocation {
            let myLat = mylocation.coordinate.latitude
            let myLong = mylocation.coordinate.longitude
            UserDefaults.standard.set(myLat, forKey: "myLocationLat")
            UserDefaults.standard.set(myLong, forKey: "myLocationLong")
            let camera = GMSCameraPosition.camera(withLatitude: myLat, longitude: myLong, zoom: 9)
            mapView.camera = camera
            print("User's location is know")
            locationManager.stopUpdatingLocation()
        } else {
            print("User's location is unknown")
        }
    }
    
    @objc func setImage(sender: AnyObject) {
        if isPhotos[sender.tag] == true {
            //            delete photo
            let alert = UIAlertController(title: nil, message: "Delete this photo?", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) in
//                self.photosBtn[sender.tag].setImage(self.addPhoto, for: .normal)
                self.photosImage[sender.tag].image = self.addPhoto
                self.isPhotos[sender.tag] = false
            })
            let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            alert.addAction(ok)
            alert.addAction(cancel)
            present(alert, animated: true, completion: nil)
            return
        }
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let camera = UIAlertAction(title: "Take a photo", style: .default) { (action) in
            self.takePhotoCamera(index: sender.tag)
        }
        let gallery = UIAlertAction(title: "Select from gallery", style: .default) { (action) in
            self.takePhotoGallery(index: sender.tag)
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        camera.setValue(0, forKey: "titleTextAlignment")
        gallery.setValue(0, forKey: "titleTextAlignment")
        alert.addAction(camera)
        alert.addAction(gallery)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func takePhotoCamera(index: Int) {
        imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera
        indexButton = index
        present(imagePicker, animated: true, completion: nil)
    }
    
    func takePhotoGallery(index: Int) {
        imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        indexButton = index
        present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        imagePicker.dismiss(animated: true, completion: nil)
        isPhotos[indexButton] = true
        let photo = UIImage(data: image.lowestQualityJPEGNSData)?.resizeImage(newWidth: 640)
//        photosBtn[indexButton].setImage(photo, for: .normal)
        self.photosImage[indexButton].image = photo
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ProductInfo" {
            let vc = segue.destination as? ProductFieldInfoVC
            self.productInfo = vc
        }
        if segue.identifier == "ProductLocation" {
            let vc = segue.destination as? ProductFieldLocation
            self.productLocation = vc
        }
    }
    
    @objc func Submit(sender: AnyObject) {
        self.view.endEditing(true)
        let isUserLoggedIn = UserDefaults.standard.bool(forKey: "isUserLoggedIn")
        if !isUserLoggedIn {
            let alert = UIAlertController(title: nil, message: "Please login first!", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
            return
        }
        let id = UserDefaults.standard.integer(forKey:"userId")
        let nameProduct = productInfo.nameProduct.text!
        let categoryName = productInfo.categorySelected
        let category = productInfo.categoryId
        let price = productInfo.priceProduct.text!
        let description = productInfo.descriptionProduct.text!
        let locationName = productLocation.location
        var lat = Double()
        var long = Double()
        if let mylocation = mapView.myLocation {
            print("location found")
            lat = mylocation.coordinate.latitude
            long = mylocation.coordinate.longitude
        } else {
            print("location not found")
            let alert = UIAlertController(title: nil, message: "Wait until your location found!", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
            return
        }
        
        var photo_encode = [Any]()
        
        if nameProduct.isEmpty || categoryName.isEmpty || price.isEmpty || description.isEmpty || locationName.isEmpty {
            let alert = UIAlertController(title: nil, message: "All fields are required!", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
            return
        }
        
        if !isPhotos[0] || !isPhotos[2] || !isPhotos[3] || !isPhotos[4] {
            let alert = UIAlertController(title: nil, message: "Please complete main photos!", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
            return
        }
        
        photo_encode.removeAll()
        for i in 0 ..< photosBtn.count {
            if isPhotos[i] {
//                let image = photosBtn[i].currentImage!
                let image = photosImage[i].image!
                let imageData = image.pngData()
                let base64String = imageData!.base64EncodedString(options: .lineLength64Characters)
                let object = convertJsonString(name: photoName[i], photos: base64String)!
                photo_encode.append(object)
            }
        }
        
//        let photo = jsonArray()
        
        self.postSuggest(id: id, name: nameProduct, idCategory: category, description: description,
                         price: price, lat: lat, long: long, photos: String(describing: photo_encode))
        
    }
    
    func postSuggest(id: Int, name: String, idCategory: Int, description: String, price: String, lat: Double, long: Double, photos: String) {
        self.activityId.startAnimating()
        self.submitBtn.isEnabled = false
        let parameters: [String : Any] = [
            "id_member": id,
            "name": name,
            "id_productcategory": idCategory,
            "description": description,
            "price": price,
            "lat": lat,
            "long": long,
            "photos": photos
        ]
        let headers = [
            "User-Agent": "iphone",
            "Referer": "CROCODICHEBAT"
        ]
        let url = URL(string: Config().BASE_API_CUSTOM + Config().ADD_SUGGEST_PRODUCT + Config().SecretKey())!
        Alamofire.request(url, method: .post, parameters: parameters, headers: headers)
            .responseJSON { (response) in
                if response.result.isSuccess {
                    let json = try! JSON(data:response.data!)
                    let message = json["api_message"].stringValue
                    let status = json["api_status"].intValue
                    let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
                    if status == 1 {
                        let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                            for i in 0 ..< self.photosBtn.count {
                                if self.isPhotos[i] {
//                                    self.photosBtn[i].setImage(self.addPhoto, for: .normal)
                                    self.photosImage[i].image = self.addPhoto
                                    self.isPhotos[i] = false
                                }
                            }
                            self.productInfo.Reset()
                            self.productLocation.Reset()
                        })
                        alert.addAction(ok)
                    } else {
                        let ok = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                        alert.addAction(ok)
                    }
                    self.present(alert, animated: true, completion: nil)
                } else {
                    
                }
                
                self.activityId.stopAnimating()
                self.submitBtn.isEnabled = true
        }.resume()
    }
    
    func convertJsonString(name: String, photos: String) -> String?{
        let para:NSMutableDictionary = NSMutableDictionary()
        para.setValue(name, forKey: "name")
        para.setValue(photos, forKey: "photo_encode")
        // let jsonError: NSError?
        let jsonData: Data
        do{
            jsonData = try JSONSerialization.data(withJSONObject: para, options: JSONSerialization.WritingOptions())
            let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            return jsonString
        } catch _ {
            print ("UH OOO")
            print("hai aku berhasil di load pakek json loh")
            return nil
        }
    }
}
