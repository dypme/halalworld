//
//  ContactVC.swift
//  HalalWorld
//
//  Created by Crocodic Studio on 4/21/16.
//  Copyright © 2016 Crocodic Studio. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire
import SwiftyJSON

class ContactVC: UIViewController {

    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var phone: UILabel!
    
    var locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        // Do any additional setup after load"ing the view.
        let camera = GMSCameraPosition.camera(withLatitude: 36.204824, longitude: 138.252924, zoom: 4)
        self.mapView.camera = camera
        
        self.post(name: ["phone", "email_sender", "address", "longitude", "latitude"])
        
    }

    func post(name: [String]) {
        let headers = [
            "User-Agent": "iphone",
            "Referer": "CROCODICHEBAT"
        ]
        
        let name = name
        
        let url = URL(string: Config().BASE_URL + Config().GET_CONTACT + Config().SecretKey())!
        Alamofire.request(url, method: .post, parameters: ["name": name], headers: headers)
            .responseJSON { (response) in
                if response.result.isSuccess {
                    let json = try! JSON(data:response.data!)
                    let status = json["api_status"].intValue
                    if status == 1 {
                        let lat = json["data"][3]["content"].doubleValue
                        let long = json["data"][2]["content"].doubleValue
                        let address = json["data"][1]["content"].stringValue
                        let email = json["data"][4]["content"].stringValue
                        let phone = json["data"][0]["content"].stringValue
                        self.setup(lat: lat, long: long, address: address, email: email, phone: phone)
                    } else {
                        return self.post(name: name)
                    }
                } else {
                    return self.post(name: name)
                }
        }
    }
    
    func setup(lat: Double, long: Double, address: String, email: String, phone: String) {
        let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: long, zoom: 9)
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2DMake(lat, long)
        marker.snippet = "Our Location"
        marker.appearAnimation = GMSMarkerAnimation.pop
        marker.map = self.mapView
        marker.icon = UIImage(named: "pin_map.png")
        self.mapView.camera = camera
        self.mapView.isMyLocationEnabled = true
        self.location.text = address
        self.email.text = email
        self.phone.text = phone
    }
}
