//
//  LoginVC.swift
//  halalworld
//
//  Created by Crocodic Studio on 3/29/16.
//  Copyright © 2016 Crocodic Studio. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class LoginVC: UIViewController{

    @IBOutlet weak var container: UIView!
    var emailField: UITextField!
    @IBOutlet weak var activityInd: UIActivityIndicatorView!
    
    private var loginEmbed: LoginTableVC!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        
        container.layer.cornerRadius = 5
        container.clipsToBounds = true
        
        loginEmbed.activityInd = self.activityInd
        
        let image = UIImage(named: "logo_halal_world_2.png")
        let height = CGFloat((self.navigationController?.navigationBar.frame.height)!)
        let rect = CGRect(x: 0, y: 0, width: 200, height: height)
        let titleImage = UIImageView(frame: rect)
        titleImage.image = image
        titleImage.contentMode = .scaleAspectFit
        
        self.navigationItem.titleView = titleImage
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "loginEmbed" {
            let vc = segue.destination as? LoginTableVC
            self.loginEmbed = vc
        }
    }
    
    @IBAction func forgetPwBtn(sender: AnyObject) {
        //Forgot password here
        let alert = UIAlertController(title: nil, message: "Please input your email!", preferredStyle: .alert)
        
        alert.addTextField { (textField) in
            self.emailField = textField
            self.emailField.placeholder = "Email"
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let ok = UIAlertAction(title: "OK", style: .default) { (action) in
            self.loading(loading: true)
            self.forgotPassword()
        }
        alert.addAction(cancel)
        alert.addAction(ok)
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func forgotPassword() {
        self.view.endEditing(true)
        let userEmail = emailField.text!
        
        let headers = [
            "User-Agent": "iphone",
            "Referer": "CROCODICHEBAT"
        ]
        
        let url = URL(string: Config().BASE_URL + Config().FORGOT_PASSWORD + Config().SecretKey())!
        Alamofire.request(url, method: .post, parameters: ["email": userEmail], headers: headers)
            .responseJSON { (response) in
                    if response.result.isSuccess {
                        let json = try! JSON(data:response.data!)
                        let message = json["api_message"].stringValue
                        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
                        let ok = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                        alert.addAction(ok)
                        self.present(alert, animated: true, completion: nil)
                        self.loading(loading: false)
                }
        }
    }
    
    func loading(loading: Bool) {
        if loading {
            activityInd.startAnimating()
        } else {
            activityInd.stopAnimating()
        }
    }
    
}
