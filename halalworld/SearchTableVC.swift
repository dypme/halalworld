//
//  SearchTableViewController.swift
//  halalworld
//
//  Created by Crocodic Studio on 4/1/16.
//  Copyright © 2016 Crocodic Studio. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Kingfisher
import GoogleMaps

class SearchTableVC: UITableViewController, GMSMapViewDelegate, TableViewCellDelegate {

    var productDatas = [DataList]()
    var placeDatas = [DataList]()
    var datas = [[DataList]]()
    var filtered = [[DataList]]()
    var titleData = ["Product", "Place"]
    var tableTitle = ["Product", "Place"]
    var placeId = Bool()
    var productId = Bool()
    
    var searchController : UISearchController!
    
    let activityIndicator = UIActivityIndicatorView(style: .white)
    
    var label = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: "TableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "cell")
        tableView.estimatedRowHeight = 30;
        tableView.rowHeight = UITableView.automaticDimension
        
        activityIndicator.color = UIColor.black
        activityIndicator.hidesWhenStopped = true
        
        label.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
        label.text = "Data Not Found"
        label.textAlignment = .center
        label.isHidden = true
        view.addSubview(label)
        
        self.view.addSubview(activityIndicator)
    }
    
    override func viewDidLayoutSubviews() {
        label.center = self.view.center
        activityIndicator.center = self.view.center
    }
    
    func indicator(filterProductDatas: [DataList], filterPlaceDatas: [DataList], searchText: String) {
        activityIndicator.startAnimating()
        self.label.isHidden = true
        if !placeId && !productId {
            activityIndicator.stopAnimating()
            self.tableView.separatorStyle = .singleLine
            if filterProductDatas.count > 0 && filterPlaceDatas.count > 0 {
                tableTitle = ["Product", "Place"]
                filtered = [filterProductDatas, filterPlaceDatas]
            } else if filterProductDatas.count > 0{
                tableTitle = ["Product"]
                filtered = [filterProductDatas]
            } else if filterPlaceDatas.count > 0{
                tableTitle = ["Place"]
                filtered = [filterPlaceDatas]
            } else {
                self.tableView.separatorStyle = .none
                tableTitle = []
                filtered = []
                self.label.isHidden = false
            }
            self.tableView.reloadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        productId = true
        placeId = true
    }
    
    func searchPlace(searchText: String, isProduct: Bool) {
        let id_member = UserDefaults.standard.integer(forKey:"userId")
        let parameters = [
            "id_member": id_member,
            "search_name": searchText
            ] as [String : Any]
        
        let headers = [
            "User-Agent": "iphone",
            "Referer": "CROCODICHEBAT"
        ]
        
        let url = URL(string: Config().BASE_URL + Config().SEARCH_PLACE + Config().SecretKey())!
        
        Alamofire.request(url, method: .post,parameters: parameters, headers: headers)
            .responseJSON { (response) in
                if response.result.isSuccess {
                    let json = try! JSON(data:response.data!)
                    let status = json["api_status"].intValue
                    self.placeDatas.removeAll()
                    if status == 1 {
                        let dataTotal = json["api_total_data"].intValue
                        for i in 0 ..< dataTotal {
                            let id = json["data"][i]["id"].intValue
                            let favoriteId = json["data"][i]["id_favorite"].intValue
                            let name = json["data"][i]["name"].stringValue
                            let description = json["data"][i]["description"].stringValue
                            let category = json["data"][i]["placecategory_name"].stringValue
                            let photo = json["data"][i]["url_photo"].stringValue
                            let favorite = json["data"][i]["is_favorite"].intValue != 0 ? true : false
                            let lat = json["data"][i]["lat"].doubleValue
                            let long = json["data"][i]["long"].doubleValue
                            let appstore = json["data"][i]["appstore"].stringValue
                            let coordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
                            self.placeDatas.append(
                                DataList(
                                    id: id,
                                    favoriteId: favoriteId,
                                    name: name,
                                    description: description,
                                    category: category,
                                    photo: photo,
                                    direction: "",
                                    favorite: favorite,
                                    appstore: appstore,
                                    coordinate: coordinate
                                ))
                        }
                    }
                } else {
                    
                }
                self.placeId = false
//                self.filterContentForSearchText(self.searchController.searchBar.text!)
                self.indicator(filterProductDatas: self.productDatas, filterPlaceDatas: self.placeDatas, searchText: searchText)
                self.activityIndicator.stopAnimating()
                if self.filtered.isEmpty {
                    self.label.isHidden = !self.activityIndicator.isHidden
                }
        }
    }
    
    func searchProduct(searchText: String) {
        activityIndicator.startAnimating()
        label.isHidden = !activityIndicator.isHidden
        let id_member = UserDefaults.standard.integer(forKey:"userId")
        let parameters = [
            "id_member": id_member,
            "search_name": searchText
            ] as [String : Any]
        
        let headers = [
            "User-Agent": "iphone",
            "Referer": "CROCODICHEBAT"
        ]
        
        let url = URL(string: Config().BASE_URL + Config().SEARCH_PRODUCT + Config().SecretKey())!
        Alamofire.request(url, method: .post,parameters: parameters, headers: headers)
            .responseJSON { (response) in
                if response.result.isSuccess {
                    let json = try! JSON(data:response.data!)
                    let status = json["api_status"].intValue
                    self.productDatas.removeAll()
                    if status == 1 {
                        let dataTotal = json["api_total_data"].intValue
                        for i in 0 ..< dataTotal {
                            let id = json["data"][i]["id"].intValue
                            let favoriteId = json["data"][i]["id_favorite"].intValue
                            let name = json["data"][i]["name"].stringValue
                            let description = json["data"][i]["description"].stringValue
                            let category = json["data"][i]["productcategory_name"].stringValue
                            let photo = json["data"][i]["url_photo"].stringValue
                            let favorite = json["data"][i]["is_favorite"].intValue != 0 ? true : false
                            let lat = json["data"][i]["place_lat"].doubleValue
                            let long = json["data"][i]["place_long"].doubleValue
                            let appstore = json["data"][i]["appstore"].stringValue
                            let coordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
                            self.productDatas.append(
                                DataList(
                                    id: id,
                                    favoriteId: favoriteId,
                                    name: name,
                                    description: description,
                                    category: category,
                                    photo: photo,
                                    direction: "",
                                    favorite: favorite,
                                    appstore: appstore,
                                    coordinate: coordinate
                                ))
                        }
                        self.searchPlace(searchText: searchText, isProduct: true)
                    } else {
                        self.searchPlace(searchText: searchText, isProduct: false)
                    }
                } else {
                    
                }
                self.productId = false
        }
    }
    
    func filterContentForSearchText(searchText: String, scope: String = "All") {
        var filteredProduct = [DataList]()
        var filteredPlace = [DataList]()
        datas = [productDatas, placeDatas]
        if searchText.isEmpty {
            filteredProduct = productDatas
            filteredPlace = placeDatas
        } else {
            filteredProduct = productDatas.filter { filter in
                return filter.name.lowercased().contains(searchText.lowercased())
            }
            filteredPlace = placeDatas.filter { filter in
                return filter.name.lowercased().contains(searchText.lowercased())
            }
        }
        self.indicator(filterProductDatas: filteredProduct, filterPlaceDatas: filteredPlace, searchText: searchText)
    }
    
    func shareBtn(cell: TableViewCell) {
        let indexPath = tableView.indexPathForRow(at: cell.center)!
        let placeName = self.datas[indexPath.section][indexPath.row].name
        let placeDescription = self.datas[indexPath.section][indexPath.row].description.isEmpty ? "-" : self.datas[indexPath.section][indexPath.row].description.trunc(length: 30)
        let shareUrl = self.datas[indexPath.section][indexPath.row].appstore
        let photo = self.datas[indexPath.section][indexPath.row].photo
        
        let url = URL(string: photo)!
        ImageCache.default.retrieveImage(forKey: photo, options: nil, completionHandler: { (image, cacheType) in
            if let image = image {
                let objectsToShare = [image, placeName, placeDescription, shareUrl] as [Any]
                let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                self.present(activityVC, animated: true, completion: nil)
            } else {
                ImageDownloader.default.downloadImage(with: url, progressBlock: nil, completionHandler: { (image, error, imageURL, originalData) in
                    if error == nil {
                        ImageCache.default.store(image!, forKey: photo)
                        let objectsToShare = [image!, placeName, placeDescription, shareUrl] as [Any]
                        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                        self.present(activityVC, animated: true, completion: nil)
                    }
                })
            }
        })
    }
    
    func favoriteBtn(cell: TableViewCell) {
        let indexPath = tableView.indexPathForRow(at: cell.center)!
        let isUserLoggedIn = UserDefaults.standard.bool(forKey: "isUserLoggedIn")
        if !isUserLoggedIn {
            return
        }
        var datas = [[DataList]]()
        if searchController.searchBar.text!.isEmpty || !searchController.isActive {
            datas = self.datas
        } else {
            datas = self.filtered
        }
        if datas[indexPath.section][indexPath.row].favorite {
            self.datas[indexPath.section][indexPath.row].favorite = false
            cell.favoriteView.image = cell.favoriteOff
            removeFavorite(cell: cell, indexPath: indexPath)
        } else {
            self.datas[indexPath.section][indexPath.row].favorite = true
            cell.favoriteView.image = cell.favoriteOn
            addFavorite(cell: cell, indexPath: indexPath)
        }
    }
    
    func removeFavorite(cell: TableViewCell, indexPath: IndexPath) {
        cell.favoriteBtn.isEnabled = false
        var datas = [[DataList]]()
        if searchController.searchBar.text!.isEmpty || !searchController.isActive {
            datas = self.datas
        } else {
            datas = self.filtered
        }
        let id = datas[indexPath.section][indexPath.row].favoriteId
        let headers = [
            "User-Agent": "iphone",
            "Referer": "CROCODICHEBAT"
        ]
        
        var favoriteLink = String()
        if tableTitle[indexPath.section] == "Product" {
            favoriteLink = Config().ADD_FAVORITE_PRODUCT
        } else {
            favoriteLink = Config().ADD_FAVORITE_PLACE
        }
        
        let url = URL(string: Config().BASE_URL + favoriteLink + Config().SecretKey())!
        Alamofire.request(url, method: .post, parameters: ["id": id], headers: headers)
            .responseJSON { (response) in
                if response.result.isSuccess {
                    let json = try! JSON(data:response.data!)
                    let status = json["api_status"].intValue
                    if status == 1 {
                    } else {
                        
                    }
                } else {
                    
                }
                cell.favoriteBtn.isEnabled = true
        }
    }
    
    func addFavorite(cell: TableViewCell, indexPath: IndexPath) {
        cell.favoriteBtn.isEnabled = false
        var datas = [[DataList]]()
        if searchController.searchBar.text!.isEmpty || !searchController.isActive {
            datas = self.datas
        } else {
            datas = self.filtered
        }
        let id = datas[indexPath.section][indexPath.row].id
        let idMember = UserDefaults.standard.integer(forKey:"userId")
        
        var favoriteLink = String()
        var param = String()
        var placepro = String()
        if tableTitle[indexPath.section] == "Product" {
            favoriteLink = Config().ADD_FAVORITE_PRODUCT
            placepro = "product"
            param = "id_product"
        } else {
            favoriteLink = Config().ADD_FAVORITE_PLACE
            placepro = "place"
            param = "id_place"
        }
        
        let parameters = [
            "id_member": idMember,
            "placepro": placepro,
            param: id
            ] as [String : Any]
        
        let headers = [
            "User-Agent": "iphone",
            "Referer": "CROCODICHEBAT"
        ]
        
        let url = URL(string: Config().BASE_URL + favoriteLink + Config().SecretKey())!
        Alamofire.request(url, method: .post, parameters: parameters, headers: headers)
            .responseJSON { (response) in
                if response.result.isSuccess {
                    let json = try! JSON(data:response.data!)
                    let status = json["api_status"].intValue
                    if status == 1 {
                        self.tableView.reloadData()
                    } else {
                        
                    }
                } else {
                    
                }
                
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        if searchController.isActive && !(searchController.searchBar.text?.isEmpty)!{
            return filtered.count
        }
        return datas.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.isActive && !(searchController.searchBar.text?.isEmpty)! {
            return filtered[section].count
        }
        return datas[section].count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! TableViewCell
        cell.delegate = self
        let data: DataList
        
        if searchController.isActive && !(searchController.searchBar.text?.isEmpty)!{
            data = filtered[indexPath.section][indexPath.row]
        } else {
            data = datas[indexPath.section][indexPath.row]
        }
        
        let name = data.name
        let category = data.category
        let photo = data.photo
        let direction = data.direction
        let favorite = data.favorite
        let coordinate = data.coordinate
        
        let url = URL(string: photo)!
        cell.photoImage.kf.setImage(with: url) { (image, error, cacheType, imageURL) in
            cell.photoImage.image = cell.photoImage.image?.circle
            cell.activityId.stopAnimating()
        }
        cell.configureData(name: name, category: category, distance: direction, favorite: favorite)
        cell.getDirection(targetLocation: coordinate)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return tableTitle[section]
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 45))
        let label = UILabel(frame: CGRect(x: 50, y: 0, width: tableView.frame.size.width, height: headerView.frame.size.height))
        let imageView = UIImageView(frame: CGRect(x: 17.5, y: 12.5, width: 20, height: 20))
        label.font = UIFont(name: "Raleway-Bold", size: 14)
        label.textColor = UIColor.white
        if searchController.isActive && (searchController.searchBar.text?.isEmpty)! {
            label.text = titleData[section]
        } else {
            if tableTitle.count > 0 {
                label.text = tableTitle[section]
            }
        }
        if label.text == "Product" {
            imageView.image = UIImage(named: "product_icon_44.png")
        } else {
            imageView.image = UIImage(named: "place_icon_44.png")
        }
        headerView.addSubview(label)
        headerView.addSubview(imageView)
        headerView.backgroundColor = UIColor(red: 63/255, green: 131/255, blue: 178/255, alpha: 1)
        
        return headerView
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 45.0
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data: DataList
        
        if searchController.isActive && !(searchController.searchBar.text?.isEmpty)!{
            data = filtered[indexPath.section][indexPath.row]
        } else {
            data = datas[indexPath.section][indexPath.row]
        }
        let id = data.id
        let favoriteId = data.favoriteId
        let isFavorite = data.favorite
        let mainStoryBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        var vc = UIViewController()
        if indexPath.section == 0 {
            UserDefaults.standard.set(favoriteId, forKey: "favoriteProductId")
            UserDefaults.standard.set(id, forKey: "productId")
            UserDefaults.standard.set(isFavorite, forKey: "isFavorite")
            vc = mainStoryBoard.instantiateViewController(withIdentifier: "DetailProduct")
        } else if indexPath.section == 1 {
            UserDefaults.standard.set(favoriteId, forKey: "favoritePlaceId")
            UserDefaults.standard.set(id, forKey: "placeId")
            UserDefaults.standard.set(isFavorite, forKey: "isFavorite")
            vc = mainStoryBoard.instantiateViewController(withIdentifier: "DetailPlace")
        }
        self.present(vc, animated: true, completion: nil)
    }
    
    func currentSellectedRowIndex() -> Int {
        let selectedIndexPath: IndexPath = self.tableView.indexPathForSelectedRow!
        return selectedIndexPath.row
    }
}
