//
//  CreditTableVC.swift
//  HalalWorld
//
//  Created by Crocodic Studio on 5/18/16.
//  Copyright © 2016 Crocodic Studio. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Kingfisher

class CreditTableVC: UITableViewController {

    let cellIdentifier = "cellCredit"
    
    var listCredit = [Credit]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.estimatedRowHeight = 50
        tableView.rowHeight = UITableView.automaticDimension
        
        self.postCredit()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return listCredit.count
    }

    func postCredit() {
        let headers = [
            "User-Agent": "iphone",
            "Referer": "CROCODICHEBAT"
        ]
        
        let url = URL(string: Config().BASE_URL + Config().LIST_CREDIT + Config().SecretKey())!
        Alamofire.request(url, method: .post, headers: headers)
            .responseJSON { (response) in
                if response.result.isSuccess {
                    let json = try! JSON(data:response.data!)
                    let status = json["api_status"].intValue
                    if status == 1 {
                        self.listCredit.removeAll()
                        let dataTotal = json["api_total_data"].intValue
                        for i in 0 ..< dataTotal {
                            let content = json["data"][i]["content"].stringValue
                            let url_photo = json["data"][i]["url_photo"].stringValue
                            self.listCredit.append(Credit(urlPhoto: url_photo, content: content))
                        }
                    } else {
                        return self.postCredit()
                    }
                } else {
                    return self.postCredit()
                }
                self.tableView.reloadData()
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! CreditCell
        
        let content = listCredit[indexPath.row].content
        let photo = listCredit[indexPath.row].urlPhoto
        
        cell.content.text = content
        
        let url = URL(string: photo)!
        cell.photo.kf.setImage(with: url) { (image, error, cacheType, imageURL) in
            cell.photo.contentMode = .scaleAspectFit
            cell.activityId.stopAnimating()
        }
        
        return cell
    }


}
