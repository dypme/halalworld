//
//  ProfileTableVC.swift
//  HalalWorld
//
//  Created by Crocodic Studio on 4/13/16.
//  Copyright © 2016 Crocodic Studio. All rights reserved.
//

import UIKit

class ProfileTableVC: UITableViewController {

    @IBOutlet weak var name: LabelInset!
    @IBOutlet weak var address: LabelInset!
    @IBOutlet weak var age: LabelInset!
    @IBOutlet weak var gender: LabelInset!
    @IBOutlet weak var email: LabelInset!
    @IBOutlet weak var phone: LabelInset!
    @IBOutlet weak var country: LabelInset!
    
}
