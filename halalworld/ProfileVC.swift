//
//  ProfileVC.swift
//  halalworld
//
//  Created by Crocodic Studio on 4/6/16.
//  Copyright © 2016 Crocodic Studio. All rights reserved.
//

import UIKit

class ProfileVC: UIViewController {
    
    @IBOutlet weak var logoutBtn: UIButton!
    
    private var profileEmbed: ProfileTableVC!
    private var passwordEmbed: PasswordTableVC!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        logoutBtn.backgroundColor = UIColor.clear
        logoutBtn.layer.cornerRadius = 20
        logoutBtn.layer.borderWidth = 1
        logoutBtn.layer.borderColor = UIColor(red: 63/255, green: 131/255, blue: 178/255, alpha: 1).cgColor
        logoutBtn.addTarget(self, action: #selector(ProfileVC.logout(sender:)), for: .touchUpInside)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        let isUserLoggedIn = UserDefaults.standard.bool(forKey: "isUserLoggedIn")
        
        if isUserLoggedIn {
            let name = UserDefaults.standard.object(forKey: "userName")!
            let gender = UserDefaults.standard.object(forKey: "userGender")!
            let nationality = UserDefaults.standard.object(forKey: "userNationality")!
            let age = UserDefaults.standard.integer(forKey:"userAge")
            let address = UserDefaults.standard.object(forKey: "userAddress")!
            let password = UserDefaults.standard.object(forKey: "userPassword")!
            let email = UserDefaults.standard.object(forKey: "userEmail")!
            let phone = UserDefaults.standard.object(forKey: "userPhone")!
            
            logoutBtn.setTitle("LOGOUT", for: .normal)
            profileEmbed.name.text = ": " + String(describing: name)
            profileEmbed.gender.text = ": " + String(describing: gender)
            profileEmbed.country.text = ": " + String(describing: nationality)
            profileEmbed.age.text = ": " + String(describing: age) + " years old"
            profileEmbed.address.text = ": " + String(describing: address)
            profileEmbed.phone.text = ": " + String(describing: phone)
            profileEmbed.email.text = ": " + String(describing: email)
            passwordEmbed.password.text = String(describing: password)
        } else {
            logoutBtn.setTitle("LOGIN", for: .normal)
            profileEmbed.name.text = ": -"
            profileEmbed.gender.text = ": -"
            profileEmbed.country.text = ": -"
            profileEmbed.age.text = ": -"
            profileEmbed.address.text = ": -"
            profileEmbed.phone.text = ": -"
            profileEmbed.email.text = ": -"
            passwordEmbed.password.text = ""
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "profileEmbed" {
            let vc = segue.destination as? ProfileTableVC
            self.profileEmbed = vc
        }
        if segue.identifier == "passwordEmbed" {
            let vc = segue.destination as? PasswordTableVC
            self.passwordEmbed = vc
        }
    }
    
    @objc func logout(sender: AnyObject) {
        let isUserLoggedIn = UserDefaults.standard.bool(forKey: "isUserLoggedIn")
        
        if isUserLoggedIn {
            UserDefaults.standard.set(false, forKey: "isUserLoggedIn")
            UserDefaults.standard.removeObject(forKey: "userId")
            UserDefaults.standard.removeObject(forKey: "userName")
            UserDefaults.standard.removeObject(forKey: "userGender")
            UserDefaults.standard.removeObject(forKey: "userNationality")
            UserDefaults.standard.removeObject(forKey: "userAge")
            UserDefaults.standard.removeObject(forKey: "userAddress")
            UserDefaults.standard.removeObject(forKey: "userPassword")
            UserDefaults.standard.removeObject(forKey: "userPhone")
        
            logoutBtn.setTitle("LOGIN", for: .normal)
            
            profileEmbed.name.text = "-"
            profileEmbed.gender.text = "-"
            profileEmbed.country.text = "-"
            profileEmbed.age.text = "-"
            profileEmbed.address.text = "-"
            profileEmbed.phone.text = "-"
            profileEmbed.email.text = "-"
            passwordEmbed.password.text = ""
        } else {
            let mainStoryBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc:UIViewController = mainStoryBoard.instantiateViewController(withIdentifier: "Login")
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }

    @IBAction func editProfileBtn(sender: AnyObject) {
        let isUserLoggedIn = UserDefaults.standard.bool(forKey: "isUserLoggedIn")
        
        if isUserLoggedIn {
            let mainStoryBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc:UIViewController = mainStoryBoard.instantiateViewController(withIdentifier: "EditProfile")
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            let alert = UIAlertController(title: "Login", message: "Please login first!. Do you want to login now?", preferredStyle: .alert)
            let yes = UIAlertAction(title: "Yes", style: .default, handler: { (action) -> Void in
                let mainStoryBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let vc:UIViewController = mainStoryBoard.instantiateViewController(withIdentifier: "Login")
                self.navigationController?.pushViewController(vc, animated: true)
            })
            let no = UIAlertAction(title: "No", style: .cancel, handler: nil)
            alert.addAction(yes)
            alert.addAction(no)
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func editPasswordBtn(sender: AnyObject) {
        let isUserLoggedIn = UserDefaults.standard.bool(forKey: "isUserLoggedIn")
        
        if isUserLoggedIn {
            let mainStoryBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc:UIViewController = mainStoryBoard.instantiateViewController(withIdentifier: "EditPassword")
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            let alert = UIAlertController(title: "Login", message: "Please login first!. Do you want to login now?", preferredStyle: .alert)
            let yes = UIAlertAction(title: "Yes", style: .default, handler: { (action) -> Void in
                let mainStoryBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let vc:UIViewController = mainStoryBoard.instantiateViewController(withIdentifier: "Login")
                self.navigationController?.pushViewController(vc, animated: true)
            })
            let no = UIAlertAction(title: "No", style: .cancel, handler: nil)
            alert.addAction(yes)
            alert.addAction(no)
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
}
