//
//  AboutBankCell.swift
//  HalalWorld
//
//  Created by Crocodic Studio on 5/18/16.
//  Copyright © 2016 Crocodic Studio. All rights reserved.
//

import UIKit

class AboutBankCell: UITableViewCell {

    @IBOutlet var photo: UIImageView!
    @IBOutlet var name: UILabel!
    @IBOutlet var norek: UILabel!
    @IBOutlet var an: UILabel!
    @IBOutlet var activityId: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureData(name: String, norek: String, an: String) {
        self.name.text = name
        self.norek.text = norek
        self.an.text = an
    }

}
