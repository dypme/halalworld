//
//  PlaceChildTableViewController.swift
//  halalworld
//
//  Created by Crocodic Studio on 4/4/16.
//  Copyright © 2016 Crocodic Studio. All rights reserved.
//

import Foundation
import XLPagerTabStrip
import SwiftyJSON
import Alamofire
import Kingfisher
import GoogleMaps

class PlaceTableVC: UITableViewController, IndicatorInfoProvider, GMSMapViewDelegate, TableViewCellDelegate {
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
    
    let cellIdentifier = "postCell"
    var itemInfo = IndicatorInfo(title: "View")
    var tabStrip = UIButton()
    var activityId = UIActivityIndicatorView()
    
    var currentIdCategory = Int()
    var currentNameCategory = "All"
    
    var totalData = Int()
    
    var label = UILabel()
    
    init(style: UITableView.Style, itemInfo: IndicatorInfo, tabStrip: UIButton, activityId: UIActivityIndicatorView) {
        self.itemInfo = itemInfo
        self.tabStrip = tabStrip
        self.activityId = activityId
        super.init(style: style)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var datas = [DataList]()
    var categories = [Category]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "TableViewCell", bundle: Bundle.main), forCellReuseIdentifier: cellIdentifier)
        tableView.estimatedRowHeight = 30;
        tableView.rowHeight = UITableView.automaticDimension
        label.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
        label.text = "Data Not Found"
        label.textAlignment = .center
        label.center = self.view.center
        label.isHidden = true
        view.addSubview(label)
        self.postCategory()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabStrip.setTitle(self.currentNameCategory, for: .normal)
        getData()
    }
    
    override func viewDidLayoutSubviews() {
        label.center = self.view.center
    }
    
    func getData() {
        if currentIdCategory != 0 {
            self.listByCategory()
        } else {
            self.postList()
        }
        if categories.count > 0 {
            tabStrip.isEnabled = true
        } else {
            tabStrip.isEnabled = false
        }
    }
    
    func postList() {
        activityId.startAnimating()
        let headers = [
            "User-Agent": "iphone",
            "Referer": "CROCODICHEBAT"
        ]
        
        let id_member = UserDefaults.standard.integer(forKey:"userId")
        
        let url = URL(string: Config().BASE_URL + Config().LIST_PLACE + Config().SecretKey())!
        Alamofire.request(url, method: .post, parameters: ["id_member": id_member], headers: headers)
            .responseJSON { (response) in
                if response.result.isSuccess {
                    let json = try! JSON(data:response.data!)
                    let status = json["api_status"].intValue
                    if status == 1 {
                        self.datas.removeAll()
                        let dataTotal = json["api_total_data"].intValue
                        if dataTotal > 0{
                            self.label.isHidden = true
                        } else {
                            self.label.isHidden = false
                        }
                        self.totalData = dataTotal
                        for i in 0 ..< dataTotal {
                            let id = json["data"][i]["id"].intValue
                            let favoriteId = json["data"][i]["id_favorite"].intValue
                            let name = json["data"][i]["name"].stringValue
                            let description = json["data"][i]["description"].stringValue
                            let placeCategory = json["data"][i]["placecategory_name"].stringValue
                            let photo = json["data"][i]["url_photo"].stringValue
                            let favorite = json["data"][i]["is_favorite"].intValue > 0 ? true : false
                            let lat = json["data"][i]["lat"].doubleValue
                            let long = json["data"][i]["long"].doubleValue
                            let appstore = json["data"][i]["appstore"].stringValue
                            let coordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
                            self.datas.append(
                                DataList(
                                    id: id,
                                    favoriteId: favoriteId,
                                    name: name,
                                    description: description,
                                    category: placeCategory,
                                    photo: photo,
                                    direction: "",
                                    favorite: favorite,
                                    appstore: appstore,
                                    coordinate: coordinate
                                ))
                        }
                    } else {
                        self.datas.removeAll()
                        self.label.isHidden = false
                    }
                } else {
                    return self.postList()
                }
                self.activityId.stopAnimating()
                self.doRefresh()
        }
    }
    
    func listByCategory() {
        activityId.startAnimating()
        let id_member = UserDefaults.standard.integer(forKey:"userId")
        let parameters = [
            "id_placecategory": self.currentIdCategory,
            "id_member": id_member
        ]
        let headers = [
            "User-Agent": "iphone",
            "Referer": "CROCODICHEBAT"
        ]
        
        let url = URL(string: Config().BASE_URL + Config().LIST_PLACE_BY_CATEGORY + Config().SecretKey())!
        Alamofire.request(url, method: .post, parameters: parameters, headers: headers)
            .responseJSON { (response) in
                if response.result.isSuccess {
                    let json = try! JSON(data:response.data!)
                    let status = json["api_status"].intValue
                    if status == 1 {
                        self.datas.removeAll()
                        let dataTotal = json["api_total_data"].intValue
                        if dataTotal > 0{
                            self.label.isHidden = true
                        } else {
                            self.label.isHidden = false
                        }
                        self.totalData = dataTotal
                        for i in 0 ..< dataTotal {
                            let id = json["data"][i]["id"].intValue
                            let favoriteId = json["data"][i]["id_favorite"].intValue
                            let name = json["data"][i]["name"].stringValue
                            let description = json["data"][i]["description"].stringValue
                            let placeCategory = json["data"][i]["placecategory_name"].stringValue
                            let photo = json["data"][i]["url_photo"].stringValue
                            let favorite = json["data"][i]["is_favorite"].intValue > 0 ? true : false
                            let lat = json["data"][i]["lat"].doubleValue
                            let long = json["data"][i]["long"].doubleValue
                            let appstore = json["data"][i]["appstore"].stringValue
                            let coordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
                            self.datas.append(
                                DataList(
                                    id: id,
                                    favoriteId: favoriteId,
                                    name: name,
                                    description: description,
                                    category: placeCategory,
                                    photo: photo,
                                    direction: "",
                                    favorite: favorite,
                                    appstore: appstore,
                                    coordinate: coordinate
                                ))
                        }
                    } else {
                        self.datas.removeAll()
                        self.label.isHidden = false
                    }
                } else {
                    return self.listByCategory()
                }
                self.activityId.stopAnimating()
                self.doRefresh()
        }
    }
    
    func postCategory() {
        let headers = [
            "User-Agent": "iphone",
            "Referer": "CROCODICHEBAT"
        ]
        
        let url = URL(string: Config().BASE_URL + Config().PLACE_CATEGORY + Config().SecretKey())!
        Alamofire.request(url, method: .post, headers: headers)
            .responseJSON { (response) in
                if response.result.isSuccess {
                    let json = try! JSON(data:response.data!)
                    let status = json["api_status"].intValue
                    if status == 1 {
                        let dataTotal = json["api_total_data"].intValue
                        self.categories.append(Category(id: 0, name: "All"))
                        for i in 0 ..< dataTotal {
                            let id = json["data"][i]["id"].intValue
                            let name = json["data"][i]["name"].stringValue
                            self.categories.append(Category(id: id, name: name))
                        }
                        self.categories.append(Category(name: "Cancel"))
                        self.tabStrip.isEnabled = true
                    } else {
                        return self.postCategory()
                    }
                } else {
                    return self.postCategory()
                }
        }
    }
    
    func shareBtn(cell: TableViewCell) {
        let indexPath = tableView.indexPathForRow(at: cell.center)!
        let placeName = self.datas[indexPath.row].name
        let placeDescription = self.datas[indexPath.row].description.isEmpty ? "-" : self.datas[indexPath.row].description.trunc(length: 30)
        let shareUrl = self.datas[indexPath.row].appstore
        let photo = self.datas[indexPath.row].photo
        
        let url = URL(string: photo)!
        ImageCache.default.retrieveImage(forKey: photo, options: nil, completionHandler: { (image, cacheType) in
            if let image = image {
                let objectsToShare = [image, placeName, placeDescription, shareUrl] as [Any]
                let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                self.present(activityVC, animated: true, completion: nil)
            } else {
                ImageDownloader.default.downloadImage(with: url, progressBlock: nil, completionHandler: { (image, error, imageURL, originalData) in
                    if error == nil {
                        ImageCache.default.store(image!, forKey: photo)
                        let objectsToShare = [image!, placeName, placeDescription, shareUrl] as [Any]
                        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                        self.present(activityVC, animated: true, completion: nil)
                    }
                })
            }
        })
    }
    
    func favoriteBtn(cell: TableViewCell) {
        let indexPath = tableView.indexPathForRow(at: cell.center)!
        let isUserLoggedIn = UserDefaults.standard.bool(forKey: "isUserLoggedIn")
        if !isUserLoggedIn {
            return
        }
        if datas[indexPath.row].favorite {
            self.datas[indexPath.row].favorite = false
            cell.favoriteView.image = cell.favoriteOff
            removeFavorite(cell: cell, indexPath: indexPath)
        } else {
            self.datas[indexPath.row].favorite = true
            cell.favoriteView.image = cell.favoriteOn
            addFavorite(cell: cell, indexPath: indexPath)
        }
    }
    
    func removeFavorite(cell: TableViewCell, indexPath: IndexPath) {
        cell.favoriteBtn.isEnabled = false
        let id = datas[indexPath.row].favoriteId
        let headers = [
            "User-Agent": "iphone",
            "Referer": "CROCODICHEBAT"
        ]
        
        let url = URL(string: Config().BASE_URL + Config().DELETE_FAVORITE + Config().SecretKey())!
        Alamofire.request(url, method: .post, parameters: ["id": id], headers: headers)
            .responseJSON { (response) in
                if response.result.isSuccess {
                    let json = try! JSON(data:response.data!)
                    let status = json["api_status"].intValue
                    if status == 1 {
                    } else {
                        
                    }
                } else {
                    
                }
                cell.favoriteBtn.isEnabled = true
        }
    }
    
    func addFavorite(cell: TableViewCell, indexPath: IndexPath) {
        cell.favoriteBtn.isEnabled = false
        let id = datas[indexPath.row].id
        let idMember = UserDefaults.standard.integer(forKey:"userId")
        
        let parameters = [
            "id_member": idMember,
            "placepro": "place",
            "id_place": id
            ] as [String : Any]
        let headers = [
            "User-Agent": "iphone",
            "Referer": "CROCODICHEBAT"
        ]
        
        let url = URL(string: Config().BASE_URL + Config().ADD_FAVORITE_PLACE + Config().SecretKey())!
        Alamofire.request(url, method: .post, parameters: parameters, headers: headers)
            .responseJSON { (response) in
                if response.result.isSuccess {
                    let json = try! JSON(data:response.data!)
                    let status = json["api_status"].intValue
                    if status == 1 {
                        self.getData()
                    } else {
                        
                    }
                } else {
                    
                }
                cell.favoriteBtn.isEnabled = true
                
        }
    }
    
    func doRefresh(){
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datas.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! TableViewCell
        cell.delegate = self
        let photo = datas[indexPath.row].photo
        let name = datas[indexPath.row].name
        let category = datas[indexPath.row].category
        let direction = datas[indexPath.row].direction
        let favorite = datas[indexPath.row].favorite
        let coordinate = datas[indexPath.row].coordinate

        let url = URL(string: photo)!
        cell.photoImage.kf.setImage(with: url) { (image, error, cacheType, imageURL) in
            cell.photoImage.image = cell.photoImage.image?.circle
            cell.activityId.stopAnimating()
        }
        cell.configureData(name: name, category: category, distance: direction, favorite: favorite)
        cell.getDirection(targetLocation: coordinate)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let placeId = datas[indexPath.row].id
        let favoriteId = datas[indexPath.row].favoriteId
        let isFavorite = datas[indexPath.row].favorite
        UserDefaults.standard.set(favoriteId, forKey: "favoritePlaceId")
        UserDefaults.standard.set(placeId, forKey: "placeId")
        UserDefaults.standard.set(isFavorite, forKey: "isFavorite")
        
        let mainStoryBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc:UIViewController = mainStoryBoard.instantiateViewController(withIdentifier: "DetailPlace")
        self.present(vc, animated: true, completion: nil)
    }
}
