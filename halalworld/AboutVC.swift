//
//  AboutVC.swift
//  HalalWorld
//
//  Created by Crocodic Studio on 5/18/16.
//  Copyright © 2016 Crocodic Studio. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Kingfisher

class AboutVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    let cellIdentifier = "cellBank"
    
    @IBOutlet weak var aboutLabel: UILabel!
    @IBOutlet weak var tableVew: UITableView!
    
    var listBank = [Bank]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableVew.delegate = self
        tableVew.dataSource = self
        
        self.postBank()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func postBank() {
        let headers = [
            "User-Agent": "iphone",
            "Referer": "CROCODICHEBAT"
        ]
        
        let url = URL(string: Config().BASE_API_CUSTOM + Config().ABOUT + Config().SecretKey())!
        Alamofire.request(url, method: .post, headers: headers)
            .responseJSON { (response) in
                if response.result.isSuccess {
                    let json = try! JSON(data:response.data!)
                    let status = json["api_status"].intValue
                    if status == 1 {
                        self.listBank.removeAll()
                        let dataTotal = json["api_total_data"].intValue
                        let about = json["about"].stringValue
                        for i in 0 ..< dataTotal {
                            let name = json["bank"][i]["name"].stringValue
                            let norek = json["bank"][i]["norek"].stringValue
                            let an = json["bank"][i]["an"].stringValue
                            let url_photo = json["bank"][i]["url_photo"].stringValue
                            self.listBank.append(Bank(urlPhoto: url_photo, name: name, norek: norek, an: an))
                        }
                        self.aboutLabel.text = about
                    } else {
                        return self.postBank()
                    }
                } else {
                    return self.postBank()
                }
                self.tableVew.reloadData()
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return listBank.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! AboutBankCell
        
        let name = listBank[indexPath.row].name
        let norek = listBank[indexPath.row].norek
        let an = listBank[indexPath.row].an
        let photo = listBank[indexPath.row].urlPhoto
        
        let url = URL(string: photo)!
        cell.photo.kf.setImage(with: url) { (image, error, cacheType, imageURL) in
            cell.photo.contentMode = .scaleAspectFit
            cell.activityId.stopAnimating()
        }
        
        cell.configureData(name: name, norek: norek, an: an)
        
        return cell
    }

}
