//
//  SuggestVC.swift
//  HalalWorld
//
//  Created by Crocodic Studio on 4/22/16.
//  Copyright © 2016 Crocodic Studio. All rights reserved.
//

import UIKit

class SuggestVC: UIViewController {

    @IBOutlet weak var suggestPlace: UIView!
    @IBOutlet weak var suggestProduct: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        let suggestIndex = UserDefaults.standard.integer(forKey:"suggestIndex")
        if suggestIndex == 0 {
            self.navigationItem.title = "Suggest Product"
            suggestProduct.isHidden = false
            suggestPlace.isHidden = true
        } else {
            self.navigationItem.title = "Suggest Place"
            suggestPlace.isHidden = false
            suggestProduct.isHidden = true
        }
    }
    
    func showSuggest(suggestIndex: Int) {
        if suggestIndex == 0 {
            self.navigationItem.title = "Suggest Product"
            suggestProduct.isHidden = false
            suggestPlace.isHidden = true
        } else {
            self.navigationItem.title = "Suggest Place"
            suggestPlace.isHidden = false
            suggestProduct.isHidden = true
        }
    }

}
