//
//  CreditCell.swift
//  HalalWorld
//
//  Created by Crocodic Studio on 5/18/16.
//  Copyright © 2016 Crocodic Studio. All rights reserved.
//

import UIKit

class CreditCell: UITableViewCell {

    @IBOutlet var photo: UIImageView!
    @IBOutlet var content: UILabel!
    @IBOutlet var activityId: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
