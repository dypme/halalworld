//
//  Config.swift
//  HalalWorld
//
//  Created by Crocodic Studio on 4/12/16.
//  Copyright © 2016 Crocodic Studio. All rights reserved.
//

import Foundation
import UIKit

class Config {
    let BASE_URL = "http://crocodic.net/halalworld/api/standard"
    let BASE_API_CUSTOM = "http://crocodic.net/halalworld/api2/custom"
    
    let REGISTER = "/save_add/member/register_member"
    let LOGIN = "/detail/member/login_member"
    let FORGOT_PASSWORD = "/forgot/member/forgot_password"
    
    let UPDATE_MEMBER = "/save_edit/member/update_member"
    let UPDATE_PASSWORD_MEMBER = "/save_edit/member/update_password_member"
    
    let LIST_LOCATION = "/list/location/list_location"
    
    let LIST_PLACE = "/list/place/list_place"
    let LIST_NEARBY_PLACE = "/list_nearby_place"
    let LIST_PLACE_BY_LOCATION = "/list/place/list_place_by_location"
    let LIST_PLACE_BY_CATEGORY = "/list/place/list_place_by_category"
    let LIST_PRODUCT = "/list/product/list_product"
    let LIST_PRODUCT_BY_CATEGORY = "/list/product/list_product_by_category"
    
    let PLACE_CATEGORY = "/list/placecategory/place_category"
    let PRODUCT_CATEGORY = "/list/productcategory/product_category"
    
    let DETAIL_PRODUCT = "/detail/product/detail_product"
    let DETAIL_PLACE = "/detail/place/detail_place"
    
    let LIST_PHOTO_PRODUCT = "/list/product_photo/list_photo_product"
    let LIST_PHOTO_PLACE = "/list/place_photo/list_place_photo"
    
    let GET_CONTACT = "/list/settings/get_contact"
    
    let LIST_PRODUCT_FAVORITE = "/list_product_favorite"
    let LIST_PLACE_FAVORITE = "/list_place_favorite"
    
    let SEARCH_PRODUCT = "/list/product/search_product"
    let SEARCH_PLACE = "/list/place/search_place"
    
    let ADD_SUGGEST_PLACE = "/add_suggest_place"
    let ADD_SUGGEST_PRODUCT = "/add_suggest_product"
    
    let ADD_FAVORITE_PRODUCT = "/save_add/favorite/add_favorite_product"
    let ADD_FAVORITE_PLACE = "/save_add/favorite/add_favorite_place"
    let DELETE_FAVORITE = "/delete/favorite/delete_favorite"
    
    let ABOUT = "/about"
    let LIST_CREDIT = "/list/credit/list_credit"
    let HELP = "/list/help/list_help"
    
    let APIKeyServer = "AIzaSyBn39aMttl_j6iS-WVg1yrYsR87ZmSBGsU"
    
    func SecretKey() -> String {
        let date = NSDate()
        let currentTime = Int64(date.timeIntervalSince1970 * 1000)
        let secret_key = self.md5(string:"CROCODICHEBAT" + String(currentTime))
        
        let secret = "?token_code="+secret_key+"&token_time="+String(currentTime)
        
        return secret
        
    }
    
    func md5(string: String) -> String {
        var digest = [UInt8](repeating: 0, count: Int(CC_MD5_DIGEST_LENGTH))
        if let data = string.data(using: String.Encoding.utf8) {
            CC_MD5((data as NSData).bytes, CC_LONG(data.count), &digest)
        }
        
        var digestHex = ""
        for index in 0..<Int(CC_MD5_DIGEST_LENGTH) {
            digestHex += String(format: "%02x", digest[index])
        }
        
        return digestHex
    }
}
