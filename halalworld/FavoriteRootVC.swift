//
//  FavoriteRootVC.swift
//  halalworld
//
//  Created by Crocodic Studio on 4/5/16.
//  Copyright © 2016 Crocodic Studio. All rights reserved.
//

import UIKit

class FavoriteRootVC: UIViewController, UISearchResultsUpdating, UISearchBarDelegate {

    @IBOutlet weak var tab: UIView!
    @IBOutlet weak var search: UIView!
    
    private var tabEmbedController: FavoriteTabStripVC!
    private var searchEmbedController: SearchFavoriteTableVC!
    
    var searchController = UISearchController(searchResultsController:  nil)
    var searchBarItem: UIBarButtonItem!
    var titleImage = UIImageView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        search.isHidden = true
        
        searchBarItem = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(showSearchBar(sender:)))
        
        setupNavigationBar()
    }

    func setupNavigationBar() {
        searchController.searchResultsUpdater = self
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.dimsBackgroundDuringPresentation = false
        
        searchController.searchBar.delegate = self
        
        definesPresentationContext = true
        
        searchEmbedController.searchController = self.searchController
        
        let image = UIImage(named: "logo_halal_world_2.png")
        let height = CGFloat((self.navigationController?.navigationBar.frame.height)!)
        let rect = CGRect(x: 0, y: 0, width: 200, height: height)
        titleImage = UIImageView(frame: rect)
        titleImage.image = image
        titleImage.contentMode = .scaleAspectFit
        
        self.navigationItem.titleView = titleImage
        navigationItem.setRightBarButton(searchBarItem, animated: true)
    }
    
    @objc func showSearchBar (sender: AnyObject) {
        searchController.searchBar.alpha = 0
        navigationItem.titleView = searchController.searchBar
        navigationItem.setRightBarButton(nil, animated: true)
        UIView.animate(withDuration: 0.25, animations: {
            self.searchController.searchBar.alpha = 1
            }, completion: { finished in
                self.searchController.searchBar.becomeFirstResponder()
        })
    }
    
    func hideSearchBar() {
        titleImage.alpha = 0
        navigationItem.setRightBarButton(self.searchBarItem, animated: true)
        UIView.animate(withDuration: 0.25, animations: {
            self.navigationItem.titleView = self.titleImage
            self.titleImage.alpha = 1
            }, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "EmbededTab" {
            let vc = segue.destination as? FavoriteTabStripVC
            self.tabEmbedController = vc
        }
        if segue.identifier == "EmbededSearch" {
            let vc = segue.destination as? SearchFavoriteTableVC
            self.searchEmbedController = vc
        }
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        searchEmbedController.filterContentForSearchText(searchText: searchController.searchBar.text!)
        print(searchController.searchBar.text ?? "")
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        tab.alpha = 0.0
        if tab.isHidden == false {
            self.navigationItem.leftBarButtonItem = .none
        }
        search.isHidden = false
        
        let productDatas = tabEmbedController.tabProduct.datas
        let placeDatas = tabEmbedController.tabPlace.datas
        searchEmbedController.productDatas = productDatas
        searchEmbedController.placeDatas = placeDatas
        searchEmbedController.label.isHidden = true
        if productDatas.count > 0 && placeDatas.count > 0 {
            searchEmbedController.datas = [productDatas, placeDatas]
            searchEmbedController.titleData = ["Product", "Place"]
        } else if productDatas.count > 0 {
            searchEmbedController.datas = [productDatas]
            searchEmbedController.titleData = ["Product"]
        } else if placeDatas.count > 0 {
            searchEmbedController.datas = [placeDatas]
            searchEmbedController.titleData = ["Place"]
        } else {
            searchEmbedController.datas = []
            searchEmbedController.titleData = []
            searchEmbedController.label.isHidden = false
        }
        searchEmbedController.filterContentForSearchText(searchText: searchBar.text!)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.hideSearchBar()
        tabEmbedController.tabProduct.postList()
        tabEmbedController.tabPlace.postList()
        tab.alpha = 1.0
        search.isHidden = true
    }

}
