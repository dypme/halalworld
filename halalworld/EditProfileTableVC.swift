//
//  EditProfileTableVC.swift
//  HalalWorld
//
//  Created by Crocodic Studio on 4/13/16.
//  Copyright © 2016 Crocodic Studio. All rights reserved.
//

import UIKit
import SwiftyJSON

class EditProfileTableVC: UITableViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet weak var username: TextFieldInset!
    @IBOutlet weak var phone: TextFieldInset!
    @IBOutlet weak var address: TextFieldInset!
    @IBOutlet weak var age: TextFieldInset!
    @IBOutlet weak var gender: UIButton!
    @IBOutlet weak var phoneCodeBtn: UIButton!
    @IBOutlet weak var phoneCodeField: UITextField!
    @IBOutlet weak var country: UIButton!
    @IBOutlet weak var countryField: UITextField!
    
    var pickerPhone: UIPickerView!
    var pickerCountry: UIPickerView!
    
    let items = ["Male", "Female", "Cancel"]
    var countries = [String]()
    var phones = [String]()
    
    var genderSelected = String()
    
    var nationality = String()
    var phoneCode = String()
    var rowPhone = Int()
    var rowCountry = Int()
    
    override func viewDidLoad() {
        self.gender.addTarget(self, action: #selector(selectGender(sender:)), for: .touchUpInside)
        self.country.addTarget(self, action: #selector(selectCountry(sender:)), for: .touchUpInside)
        self.phoneCodeBtn.addTarget(self, action: #selector(selectPhone(sender:)), for: .touchUpInside)
        phoneCodeField.addTarget(self, action: #selector(textField(sender:)), for: .editingDidBegin)
        countryField.addTarget(self, action: #selector(textField(sender:)), for: .editingDidBegin)
        countryField.tag = 1
        
        pickerPhone = UIPickerView()
        pickerPhone.delegate = self
        pickerPhone.dataSource = self
        
        pickerCountry = UIPickerView()
        pickerCountry.delegate = self
        pickerCountry.dataSource = self
        
        let countryCode = Locale.isoCurrencyCodes
        for code in countryCode{
            let id = NSLocale.localeIdentifier(fromComponents: [NSLocale.Key.countryCode.rawValue: code])
            let name = NSLocale(localeIdentifier: "en_UK").displayName(forKey: NSLocale.Key.identifier, value: id) ?? "Country not found for code: \(code)"
            countries.append(name)
        }
        getPhoneCodeList()
        // countries.append("Cancel")
        // phones.append("Cancel")
        
        let name = UserDefaults.standard.object(forKey: "userName")!
        let gender = UserDefaults.standard.object(forKey: "userGender")!
        let nationality = UserDefaults.standard.object(forKey: "userNationality")!
        let age = UserDefaults.standard.integer(forKey:"userAge")
        let address = UserDefaults.standard.object(forKey: "userAddress")!

        self.username.text = String(describing: name)
        self.address.text = String(describing: address)
        self.age.text = String(describing: age)
        self.gender.setTitle(String(describing: gender), for: .normal)
        self.genderSelected = String(describing: gender)
        self.country.setTitle(String(describing: nationality), for: .normal)
        self.countryField.text = String(describing: nationality)
        self.nationality = String(describing: nationality)
        if self.genderSelected != "" {
            self.gender.setTitleColor(UIColor.black, for: .normal)
        } else {
            let colorDeselect = UIColor(red: 204/255, green: 204/255, blue: 204/255, alpha: 1.0)
            self.gender.setTitleColor(colorDeselect, for: .normal)
        }
        if self.nationality != "" {
            self.country.setTitleColor(UIColor.black, for: .normal)
        } else {
            let colorDeselect = UIColor(red: 204/255, green: 204/255, blue: 204/255, alpha: 1.0)
            self.country.setTitleColor(colorDeselect, for: .normal)
        }
    }
    
    func getPhoneCodeList() {
        let path = Bundle.main.path(forResource: "phone", ofType: "json")!
        let data = try! Data(contentsOf: URL(string: path)!)
        let countryCode = Locale.isoCurrencyCodes
        let json = try! JSON(data:data)
        for code in countryCode {
            let newCode = json[code].stringValue
            if !newCode.isEmpty {
                phones.append("+\(newCode)")
            } else {
                let id = NSLocale.localeIdentifier(fromComponents: [NSLocale.Key.countryCode.rawValue: code])
                let name = NSLocale(localeIdentifier: "en_UK").displayName(forKey: NSLocale.Key.identifier, value: id) ?? "Country not found for code: \(code)"
                if countries.contains(name) {
                    countries = countries.filter{$0 != name}
                }
            }
        }
    }
    
    @objc func selectPhone(sender: AnyObject) {
        if phones.count <= 0 {
            return
        }
        self.view.endEditing(true)
        if self.nationality != "" {
            self.phoneCodeBtn.isHighlighted = false
        } else {
            self.phoneCodeBtn.isHighlighted = true
        }
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        for i in 0 ..< phones.count {
            var sheet = UIAlertAction()
            if i == phones.count - 1 {
                sheet = UIAlertAction(title: phones[i], style: .cancel, handler: nil)
            } else {
                sheet = UIAlertAction(title: "\(phones[i]) (\(countries[i]))", style: .default) { (action) -> Void in
                    self.phoneCodeBtn.setTitle(self.phones[i], for: .normal)
                    self.phoneCodeBtn.isHighlighted = false
                    self.phoneCode = self.phones[i]
                }
                sheet.setValue(0, forKey: "titleTextAlignment")
            }
            actionSheet.addAction(sheet)
        }
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    @objc func selectCountry(sender: AnyObject) {
        self.view.endEditing(true)
        if self.nationality != "" {
            self.country.setTitleColor(UIColor.black, for: .normal)
        } else {
            let colorDeselect = UIColor(red: 204/255, green: 204/255, blue: 204/255, alpha: 1.0)
            self.country.setTitleColor(colorDeselect, for: .normal)
        }
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        for i in 0 ..< countries.count {
            var sheet = UIAlertAction()
            sheet = UIAlertAction(title: countries[i], style: .default) { (action) -> Void in
                self.country.setTitle(self.countries[i], for: .normal)
                self.country.isHighlighted = true
                self.nationality = self.countries[i]
            }
            sheet.setValue(0, forKey: "titleTextAlignment")
            actionSheet.addAction(sheet)
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        actionSheet.addAction(cancel)
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    @objc func textField(sender: UITextField) {
        var picker = UIPickerView()
        if sender.tag == 0 {
            picker = pickerPhone
        } else {
            picker = pickerCountry
        }
        let tintColor: UIColor = UIColor(red: 101.0/255.0, green: 98.0/255.0, blue: 164.0/255.0, alpha: 1.0)
        let inputView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 240))
        picker.tintColor = tintColor
        picker.center.y = inputView.center.y + 10
        picker.center.x = inputView.center.x + 20
        inputView.addSubview(picker)
        
        let doneButton = UIButton(frame: CGRect(x: self.view.frame.size.width - 50, y: 0, width: 100, height: 50))
        doneButton.setTitleColor(UIColor.blue, for: .normal)
        doneButton.setTitle("Done", for: UIControl.State.normal)
        inputView.addSubview(doneButton) // add Button to UIView
        doneButton.addTarget(self, action: #selector(donePicker(sender:)), for: UIControl.Event.touchUpInside) // set button click event
        doneButton.tag = sender.tag
        
        let cancelButton = UIButton(frame: CGRect(x: 0, y: 0, width: 100, height: 50))
        cancelButton.setTitleColor(UIColor.red, for: .normal)
        cancelButton.setTitle("Cancel", for: UIControl.State.normal)
        inputView.addSubview(cancelButton) // add Button to UIView
        cancelButton.addTarget(self, action: #selector(cancelPicker(sender:)), for: UIControl.Event.touchUpInside) // set button click event
        cancelButton.tag = sender.tag
        
        sender.inputView = inputView
        
    }
    
    @objc func donePicker(sender: AnyObject) {
        if sender.tag == 0 {
            phoneCode = phones[rowPhone]
            phoneCodeField.text = phoneCode
            phoneCodeField.resignFirstResponder()
        } else {
            nationality = countries[rowCountry]
            countryField.text = nationality
            countryField.resignFirstResponder()
        }
        tableView.setContentOffset(CGPoint.zero, animated: false)
    }
    
    @objc func cancelPicker(sender: AnyObject) {
        if sender.tag == 0 {
            phoneCodeField.resignFirstResponder()
        } else {
            countryField.resignFirstResponder()
        }
        tableView.setContentOffset(CGPoint.zero, animated: false)
    }
    
    @objc func selectGender(sender: AnyObject) {
        self.view.endEditing(true)
        if self.genderSelected != "" {
            self.gender.setTitleColor(UIColor.black, for: .normal)
        } else {
            let colorDeselect = UIColor(red: 204/255, green: 204/255, blue: 204/255, alpha: 1.0)
            self.gender.setTitleColor(colorDeselect, for: .normal)
        }
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        for i in 0 ..< items.count {
            var sheet = UIAlertAction()
            if i == items.count-1 {
                sheet = UIAlertAction(title: items[i], style: .cancel, handler: nil)
            } else {
                sheet = UIAlertAction(title: items[i], style: .default) { (action) -> Void in
                    self.gender.setTitle(self.items[i], for: .normal)
                    self.gender.isHighlighted = true
                    self.genderSelected = self.items[i]
                }
                sheet.setValue(0, forKey: "titleTextAlignment")
            }
            actionSheet.addAction(sheet)
        }
        
        self.present(actionSheet, animated: true, completion: nil)
        
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == pickerCountry {
            return countries.count
        }
        return phones.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == pickerCountry {
            return countries[row]
        }
        return "\(phones[row]) (\(countries[row]))"
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == pickerCountry {
            rowCountry = row
        } else {
            rowPhone = row
        }
    }
}
