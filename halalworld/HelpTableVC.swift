//
//  HelpTableVC.swift
//  HalalWorld
//
//  Created by Crocodic Studio on 5/23/16.
//  Copyright © 2016 Crocodic Studio. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class HelpTableVC: UITableViewController {

    var listHelp = [Help]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = 60
        
        self.postHelp()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return listHelp.count
    }
    
    func postHelp() {
        let headers = [
            "User-Agent": "iphone",
            "Referer": "CROCODICHEBAT"
        ]
        
        let url = URL(string: Config().BASE_URL + Config().HELP + Config().SecretKey())!
        Alamofire.request(url, method: .post, headers: headers)
            .responseJSON { (response) in
                if response.result.isSuccess {
                    let json = try! JSON(data:response.data!)
                    let status = json["api_status"].intValue
                    if status == 1 {
                        self.listHelp.removeAll()
                        let dataTotal = json["api_total_data"].intValue
                        for i in 0 ..< dataTotal {
                            let title = json["data"][i]["title"].stringValue
                            let content = json["data"][i]["content"].stringValue
                            self.listHelp.append(Help(title: title, content: content))
                        }
                    } else {
                        return self.postHelp()
                    }
                } else {
                    return self.postHelp()
                }
                self.tableView.reloadData()
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! HelpCell
        
        let title = listHelp[indexPath.row].title
        
        cell.title.text = title
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let mainStoryBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc:HelpVC = mainStoryBoard.instantiateViewController(withIdentifier: "Help") as! HelpVC
        vc.helpTitleString = listHelp[indexPath.row].title
        vc.helpContentString = listHelp[indexPath.row].content
        self.navigationController?.pushViewController(vc, animated: true)
    }

}
