//
//  HomeRootVC.swift
//  halalworld
//
//  Created by Crocodic Studio on 3/31/16.
//  Copyright © 2016 Crocodic Studio. All rights reserved.
//

import UIKit

class HomeRootVC: UIViewController, UISearchBarDelegate {

    @IBOutlet weak var home: UIView!
    @IBOutlet weak var tab: UIView!
    @IBOutlet weak var search: UIView!
    
    private var tabEmbedController: ProductPlaceTabStrip!
    var homeEmbedController: HomeVC!
    private var searchEmbedController: SearchTableVC!
    
    var searchController = UISearchController(searchResultsController:  nil)
    var searchBarItem: UIBarButtonItem!
    var leftBar: UIBarButtonItem!
    var titleImage = UIImageView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tab.isHidden = true
        search.isHidden = true
        
        homeEmbedController.productBtn.addTarget(self, action: #selector(HomeRootVC.product(sender:)), for: .touchUpInside)
        homeEmbedController.placeBtn.addTarget(self, action: #selector(HomeRootVC.place(sender:)), for: .touchUpInside)
        
        searchBarItem = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(showSearchBar(sender:)))
        
        let rectLeftBtn = CGRect(x: 0, y: 0, width: 30, height: 30)
        let barButton = UIButton(frame: rectLeftBtn)
        barButton.setImage(UIImage(named: "ic_select_location"), for: .normal)
        barButton.contentMode = .scaleAspectFit
        barButton.addTarget(homeEmbedController, action: #selector(homeEmbedController.selectLocation(sender:)), for: .touchUpInside)
        leftBar = UIBarButtonItem(customView: barButton)
        homeEmbedController.leftBar = self.leftBar
        
        setupNavigationBar()
        
        let raisedController = self.tabBarController as! RaiseTabBarController
        raisedController.resetCenterButton()
    }
    
    @objc func product(sender: AnyObject) {
        self.tabEmbedController.moveToViewController(at: 0, animated: false)
        switchView()
    }
    
    @objc func place(sender: AnyObject) {
        self.tabEmbedController.moveToViewController(at: 1, animated: false)
        switchView()
    }
    
    @objc func switchView() {
        home.isHidden = !home.isHidden
        tab.isHidden = !tab.isHidden
        
        if home.isHidden == true {
            let rect = CGRect(x: 0, y: 0, width: 30, height: 30)
            let barButton = UIButton(frame: rect)
            barButton.setImage(UIImage(named: "back_icon_44.png"), for: .normal)
            barButton.contentMode = .scaleAspectFit
            barButton.addTarget(self, action: #selector(HomeRootVC.switchView), for: .touchUpInside)
            
            let leftBar = UIBarButtonItem(customView: barButton)
            self.navigationItem.leftBarButtonItem = leftBar
        } else {
            self.navigationItem.leftBarButtonItem = homeEmbedController.leftBar
        }
    }
    
    func setupNavigationBar() {
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.dimsBackgroundDuringPresentation = false
        
        searchController.searchBar.delegate = self
        
        definesPresentationContext = true
        
        searchEmbedController.searchController = self.searchController
        
        let image = UIImage(named: "logo_halal_world_2.png")
        let height = CGFloat((self.navigationController?.navigationBar.frame.height)!)
        let rect = CGRect(x: 0, y: 0, width: 200, height: height)
        titleImage = UIImageView(frame: rect)
        titleImage.image = image
        titleImage.contentMode = .scaleAspectFit
        
        navigationItem.titleView = titleImage
        navigationItem.setLeftBarButton(homeEmbedController.leftBar, animated: true)
        navigationItem.setRightBarButton(searchBarItem, animated: true)
    }
    
    @objc func showSearchBar (sender: AnyObject) {
        searchController.searchBar.alpha = 0
        navigationItem.titleView = searchController.searchBar
        navigationItem.setRightBarButton(nil, animated: true)
        UIView.animate(withDuration: 0.25, animations: {
            self.searchController.searchBar.alpha = 1
            }, completion: { finished in
                self.searchController.searchBar.becomeFirstResponder()
        })
    }
    
    func hideSearchBar() {
        titleImage.alpha = 0
        navigationItem.setRightBarButton(self.searchBarItem, animated: true)
        UIView.animate(withDuration: 0.25, animations: {
            self.navigationItem.titleView = self.titleImage
            self.titleImage.alpha = 1
            }, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "EmbededTab" {
            let vc = segue.destination as? ProductPlaceTabStrip
            self.tabEmbedController = vc
        }
        if segue.identifier == "EmbededHome" {
            let vc = segue.destination as? HomeVC
            self.homeEmbedController = vc
        }
        if segue.identifier == "EmbededSearch" {
            let vc = segue.destination as? SearchTableVC
            self.searchEmbedController = vc
        }
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        home.alpha = 0.0
        tab.alpha = 0.0
        self.navigationItem.leftBarButtonItem = .none
        search.isHidden = false
        
        searchEmbedController.productDatas.removeAll()
        searchEmbedController.placeDatas.removeAll()
        
        let filterProductDatas = searchEmbedController.productDatas
        let filterPlaceDatas = searchEmbedController.placeDatas
        searchEmbedController.tableView.separatorStyle = .singleLine
        searchEmbedController.label.isHidden = true
        if filterProductDatas.count > 0 && filterPlaceDatas.count > 0 {
            searchEmbedController.tableTitle = ["Product", "Place"]
            searchEmbedController.datas = [filterProductDatas, filterPlaceDatas]
        } else if filterProductDatas.count > 0{
            searchEmbedController.tableTitle = ["Product"]
            searchEmbedController.datas = [filterProductDatas]
        } else if filterPlaceDatas.count > 0{
            searchEmbedController.tableTitle = ["Place"]
            searchEmbedController.datas = [filterPlaceDatas]
        } else {
            searchEmbedController.tableView.separatorStyle = .none
            searchEmbedController.tableTitle = []
            searchEmbedController.datas = []
//            searchEmbedController.label.isHidden = false
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchEmbedController.searchProduct(searchText: searchBar.text!)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.hideSearchBar()
        let isNearby = UserDefaults.standard.bool(forKey: "isNearby")
        if !isNearby {
            homeEmbedController.postList()
        } else {
            let myLat = UserDefaults.standard.double(forKey: "myLocationLat")
            let myLong = UserDefaults.standard.double(forKey: "myLocationLong")
            homeEmbedController.nearbyPlace(lat: myLat, long: myLong)
        }
        tabEmbedController.tabProduct.postList()
        tabEmbedController.tabPlace.postList()
        
        home.alpha = 1.0
        tab.alpha = 1.0
        if tab.isHidden == false {
            let rect = CGRect(x: 0, y: 0, width: 30, height: 30)
            let barButton = UIButton(frame: rect)
            barButton.setImage(UIImage(named: "back_icon_44.png"), for: .normal)
            barButton.contentMode = .scaleAspectFit
            barButton.addTarget(self, action: #selector(HomeRootVC.switchView), for: .touchUpInside)
            
            let leftBar = UIBarButtonItem(customView: barButton)
            self.navigationItem.leftBarButtonItem = leftBar
        }
        else if home.isHidden == false {
            self.navigationItem.leftBarButtonItem = homeEmbedController.leftBar
        }
        search.isHidden = true
    }
}
