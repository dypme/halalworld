//
//  CellModel.swift
//  HalalWorld
//
//  Created by Crocodic Studio on 7/26/16.
//  Copyright © 2016 Crocodic Studio. All rights reserved.
//

import Foundation

class Credit {
    var urlPhoto = String()
    var content = String()
    
    init(urlPhoto: String, content: String) {
        self.urlPhoto = urlPhoto
        self.content = content
    }
}

class Bank {
    var urlPhoto = String()
    var name = String()
    var norek = String()
    var an = String()
    
    init(urlPhoto: String, name: String, norek: String, an: String) {
        self.urlPhoto = urlPhoto
        self.name = name
        self.norek = norek
        self.an = an
    }
}

class Help {
    var title = String()
    var content = String()
    
    init(title: String, content: String) {
        self.title = title
        self.content = content
    }
}