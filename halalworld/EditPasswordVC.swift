//
//  EditPasswordVC.swift
//  HalalWorld
//
//  Created by Crocodic Studio on 4/7/16.
//  Copyright © 2016 Crocodic Studio. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class EditPasswordVC: UIViewController {

    @IBOutlet weak var saveBtn: UIButton!
    
    private var editPasswordEmbed: EditPasswordTableVC!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        self.navigationController?.navigationItem.title = "Edit Password"
        
        saveBtn.backgroundColor = UIColor.clear
        saveBtn.layer.cornerRadius = 20
        saveBtn.layer.borderWidth = 1
        saveBtn.layer.borderColor = UIColor(red: 63/255, green: 131/255, blue: 178/255, alpha: 1).cgColor
        
        saveBtn.addTarget(self, action: #selector(updateProfile(sender:)), for: .touchUpInside)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "editPasswordEmbed" {
            let vc = segue.destination as? EditPasswordTableVC
            self.editPasswordEmbed = vc
        }
    }

    @objc func updateProfile(sender: AnyObject) {
        self.view.endEditing(true)
        let id = UserDefaults.standard.integer(forKey:"userId")
        let password = UserDefaults.standard.object(forKey: "userPassword")!
        let oldpassword = editPasswordEmbed.oldpassword.text!
        let newpassword = editPasswordEmbed.newpassword.text!
        let repassword = editPasswordEmbed.repassword.text!
        
        if oldpassword != String(describing: password) {
            self.dialogAlert(message: "Invalid password", status: 0)
            return
        }
        
        if newpassword != repassword {
            self.dialogAlert(message: "Password do not match!", status: 0)
            return
        }
        
        // check here for userEmail and userPassword
        let parameters = [
            "id": id,
            "password": newpassword
            ] as [String : Any]
        
        let headers = [
            "User-Agent": "iphone",
            "Referer": "CROCODICHEBAT"
        ]
        
        let url = URL(string: Config().BASE_URL + Config().UPDATE_PASSWORD_MEMBER + Config().SecretKey())!
        Alamofire.request(url, method: .post, parameters: parameters, headers: headers)
            .responseJSON { (response) in
                if response.result.isSuccess {
                    let json = try! JSON(data:response.data!)
                    let status = json["api_status"].intValue
                    self.dialogAlert(message: "\(json["api_message"].stringValue)", status: status)
                    if status == 1 {
                        UserDefaults.standard.set(password, forKey: "userPassword")
                    } else {
                        
                    }
                } else {
                    
                }
        }
    }
    
    func dialogAlert(message: String, status: Int) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        
        var ok = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        if status == 1 {
            ok = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                self.navigationController?.popViewController(animated: true)
            })
        }
        
        alert.addAction(ok)
        
        self.present(alert, animated: true, completion: nil)
    }
    
}
