//
//  Category.swift
//  HalalWorld
//
//  Created by Crocodic Studio on 7/26/16.
//  Copyright © 2016 Crocodic Studio. All rights reserved.
//

import Foundation

class Category {
    var id = Int()
    var name = String()
    
    init (id: Int, name: String) {
        self.id = id
        self.name = name
    }
    
    init (name: String) {
        self.name = name
    }
}