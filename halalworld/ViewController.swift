//
//  ViewController.swift
//  halalworld
//
//  Created by Crocodic Studio on 2/20/16.
//  Copyright © 2016 Crocodic Studio. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var registerBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loginBtn.backgroundColor = UIColor.clear
        loginBtn.layer.cornerRadius = 20
        loginBtn.layer.borderWidth = 1
        loginBtn.layer.borderColor = UIColor(red: 63/255, green: 131/255, blue: 178/255, alpha: 1).cgColor
        
        registerBtn.backgroundColor = UIColor.clear
        registerBtn.layer.cornerRadius = 20
        registerBtn.layer.borderWidth = 1
        registerBtn.layer.borderColor = UIColor(red: 63/255, green: 131/255, blue: 178/255, alpha: 1).cgColor
        
    }
    
    @IBAction func skip(sender: AnyObject) {
        let mainStoryBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc:UIViewController = mainStoryBoard.instantiateViewController(withIdentifier: "SelectLocation")
        self.present(vc, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        super.viewWillAppear(true)
        let isUserLogin = UserDefaults.standard.bool(forKey: "isUserLoggedIn")
        
        if isUserLogin {
            let mainStoryBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc:UIViewController = mainStoryBoard.instantiateViewController(withIdentifier: "SelectLocation")
            self.present(vc, animated: true, completion: nil)
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        super.viewWillDisappear(true)
    }
}

