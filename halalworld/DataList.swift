//
//  PlaceDataList.swift
//  HalalWorld
//
//  Created by Crocodic Studio on 4/15/16.
//  Copyright © 2016 Crocodic Studio. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class DataList {
    var favoriteId = Int()
    var id = Int()
    var name = String()
    var description = String()
    var category = String()
    var photo = String()
    var direction = String()
    var favorite = Bool()
    var appstore = String()
    var coordinate = CLLocationCoordinate2D()
    
    
    init(id: Int,
         favoriteId: Int,
         name: String,
         description: String,
         category: String,
         photo: String,
         direction: String,
         favorite: Bool,
         appstore: String,
         coordinate: CLLocationCoordinate2D) {
        self.id = id
        self.favoriteId = favoriteId
        self.name = name
        self.description = description
        self.category = category
        self.photo = photo
        self.direction = direction
        self.favorite = favorite
        self.appstore = appstore
        self.coordinate = coordinate
    }
}