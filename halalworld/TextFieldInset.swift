//
//  TextFieldInset.swift
//  halalworld
//
//  Created by Crocodic Studio on 3/29/16.
//  Copyright © 2016 Crocodic Studio. All rights reserved.
//

import UIKit

@IBDesignable
class TextFieldInset: UITextField {
    
        @IBInspectable var insetX: CGFloat = 0
        @IBInspectable var insetY: CGFloat = 0
        
        // placeholder position
    override func textRect(forBounds bounds: CGRect) -> CGRect {
            return bounds.insetBy(dx: insetX, dy: insetY)
        }
        
        // text position
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
            return bounds.insetBy(dx: insetX, dy: insetY)
    }
}
