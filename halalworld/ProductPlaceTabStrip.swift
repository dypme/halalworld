//
//  ExampleTabStrip.swift
//  halalworld
//
//  Created by Crocodic Studio on 3/30/16.
//  Copyright © 2016 Crocodic Studio. All rights reserved.
//

import Foundation
import XLPagerTabStrip

class ProductPlaceTabStrip: ButtonBarPagerTabStripViewController {

    @IBOutlet weak var filterBtn: UIButton!
    @IBOutlet weak var activityId: UIActivityIndicatorView!
    
    let navBarColor = UIColor(red: 53/255, green: 106/255, blue: 160/255, alpha: 1)
    
    var tabProduct: ProductTableVC! = nil
    var tabPlace: PlaceTableVC! = nil
    
    override func viewDidLoad() {
        tabProduct = ProductTableVC(style: .plain, itemInfo: IndicatorInfo(title: "Product"), tabStrip: self.filterBtn, activityId: activityId)
        tabPlace = PlaceTableVC(style: .plain, itemInfo: IndicatorInfo(title: "Place"), tabStrip: self.filterBtn, activityId: activityId)
        
        // change selected bar color
        settings.style.buttonBarBackgroundColor = UIColor.black
        settings.style.buttonBarItemBackgroundColor = navBarColor
        settings.style.selectedBarBackgroundColor = UIColor.white
        settings.style.buttonBarItemFont = UIFont.systemFont(ofSize: 14)
        settings.style.selectedBarHeight = 3
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = UIColor.white
        settings.style.buttonBarItemsShouldFillAvailiableWidth = true
        
        
        filterBtn.backgroundColor = UIColor.clear
        filterBtn.layer.cornerRadius = 20
        filterBtn.layer.borderWidth = 1
        filterBtn.layer.borderColor = UIColor(red: 63/255, green: 131/255, blue: 178/255, alpha: 1).cgColor
        
        super.viewDidLoad()
        
        self.view.layoutIfNeeded()
        buttonBarView.selectedBar.frame.origin.y = buttonBarView.frame.size.height - settings.style.selectedBarHeight
        
        filterBtn.addTarget(self, action: #selector(Filtering(sender:)), for: .touchUpInside)
    }
    
    
    
    @objc func Filtering(sender: AnyObject) {
        if currentIndex == 0 {
            self.alertSheet(categories: tabProduct.categories)
        } else if currentIndex == 1 {
            self.alertSheet(categories: tabPlace.categories)
        }
    }
    
    func alertSheet(categories: [Category]) {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        for i in 0 ..< categories.count {
            var sheet = UIAlertAction()
            if i == categories.count - 1 {
                sheet = UIAlertAction(title: categories[i].name, style: .cancel, handler: nil)
            } else {
                sheet = UIAlertAction(title: categories[i].name, style: .default) { (action) -> Void in
                    // here if filter selected
                    if self.currentIndex == 0 {
                        self.tabProduct.currentIdCategory = categories[i].id
                        self.tabProduct.currentNameCategory = categories[i].name
                        self.tabProduct.getData()
                    } else if self.currentIndex == 1 {
                        self.tabPlace.currentIdCategory = categories[i].id
                        self.tabPlace.currentNameCategory = categories[i].name
                        self.tabPlace.getData()
                    }
                    self.filterBtn.setTitle(categories[i].name, for: .normal)
                }
                sheet.setValue(0, forKey: "titleTextAlignment")
            }
            actionSheet.addAction(sheet)
        }
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    // MARK: - PagerTabStripDataSource
    
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        return [tabProduct, tabPlace]
    }

}
