//
//  InputSuggestVC.swift
//  halalworld
//
//  Created by Crocodic Studio on 3/23/16.
//  Copyright © 2016 Crocodic Studio. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire
import SwiftyJSON
import CoreLocation

class SuggestPlaceVC: UIViewController, CLLocationManagerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var mapView: GMSMapView!
    let locationManager = CLLocationManager()
    @IBOutlet weak var activityId: UIActivityIndicatorView!
    
    @IBOutlet var photosBtn: [UIButton]!
    @IBOutlet var photosImage: [UIImageView]!
    var indexButton: Int!
    
    var imagePicker = UIImagePickerController()
    var isPhotos = [false, false, false]
    var photoName = ["photo location", "photo location 2", "halal logo"]
    
    @IBOutlet weak var submitBtn: UIButton!
    
    let addPhoto = UIImage(named: "add_image_66.png")
    
    private var placeInfo: PlaceFieldInfoVC!
    private var placeLocation: PlaceFieldLocation!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.locationManager.delegate = self
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.startUpdatingLocation()
        
        let camera = GMSCameraPosition.camera(withLatitude: 36.204824, longitude: 138.252924, zoom: 4)
        self.mapView.camera = camera
        
        self.hideKeyboardWhenTappedAround()
        
        self.placeLocation.locationPlace.addTarget(self, action: #selector(EndEditing(sender:)), for: .touchUpInside)
        
        submitBtn.backgroundColor = UIColor.clear
        submitBtn.layer.cornerRadius = 20
        submitBtn.layer.borderWidth = 1
        submitBtn.layer.borderColor = UIColor(red: 63/255, green: 131/255, blue: 178/255, alpha: 1).cgColor
        submitBtn.addTarget(self, action: #selector(SuggestPlaceVC.Submit(sender:)), for: .touchUpInside)
        
        for i in 0 ..< photosBtn.count {
            photosBtn[i].addTarget(self, action: #selector(setImage(sender:)), for: .touchUpInside)
        }
        
        mapView.isMyLocationEnabled = true
    }
    
    @objc func EndEditing(sender: AnyObject) {
        self.placeInfo.view.endEditing(true)
        self.placeLocation.ShowAlert()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let mylocation = mapView.myLocation {
            let myLat = mylocation.coordinate.latitude
            let myLong = mylocation.coordinate.longitude
            UserDefaults.standard.set(myLat, forKey: "myLocationLat")
            UserDefaults.standard.set(myLong, forKey: "myLocationLong")
            let camera = GMSCameraPosition.camera(withLatitude: myLat, longitude: myLong, zoom: 9)
            mapView.camera = camera
            print("User's location is know")
            locationManager.stopUpdatingLocation()
        } else {
            print("User's location is unknown")
        }
    }
    
    @objc func setImage(sender: AnyObject) {
        if isPhotos[sender.tag] == true {
            //            delete photo
            let alert = UIAlertController(title: nil, message: "Delete this photo?", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                self.photosImage[sender.tag].image = self.addPhoto
                self.isPhotos[sender.tag] = false
            })
            let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            alert.addAction(ok)
            alert.addAction(cancel)
            present(alert, animated: true, completion: nil)
            return
        }
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let camera = UIAlertAction(title: "Take a photo", style: .default) { (action) in
            self.takePhotoCamera(index: sender.tag)
        }
        let gallery = UIAlertAction(title: "Select from gallery", style: .default) { (action) in
            self.takePhotoGallery(index: sender.tag)
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        camera.setValue(0, forKey: "titleTextAlignment")
        gallery.setValue(0, forKey: "titleTextAlignment")
        alert.addAction(camera)
        alert.addAction(gallery)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }
    
    func takePhotoCamera(index: Int) {
        imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera
        indexButton = index
        present(imagePicker, animated: true, completion: nil)
    }
    
    func takePhotoGallery(index: Int) {
        imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        indexButton = index
        present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        imagePicker.dismiss(animated: true, completion: nil)
        isPhotos[indexButton] = true
        let photo = UIImage(data: image.lowestQualityJPEGNSData)?.resizeImage(newWidth: 640)
        photosImage[indexButton].image = photo
    }
    
    @objc func Submit(sender: AnyObject) {
        self.view.endEditing(true)
        let isUserLoggedIn = UserDefaults.standard.bool(forKey: "isUserLoggedIn")
        if !isUserLoggedIn {
            let alert = UIAlertController(title: nil, message: "Please login first!", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
            return
        }
        let id = UserDefaults.standard.integer(forKey:"userId")
        let namePlace = placeInfo.namePlace.text!
        let categoryName = placeInfo.categorySelected
        let category = placeInfo.categoryId
        let description = placeInfo.descriptionPlace.text!
        let address = placeInfo.addressPlace.text!
        let phone = placeInfo.phonePlace.text!
        let hours = placeInfo.hoursPlace.text!
        let url = placeInfo.urlPlace.text!
        let locationName = placeLocation.location
        let location = placeLocation.locationId
        var lat = Double()
        var long = Double()
        if let mylocation = mapView.myLocation {
            print("location found")
            lat = mylocation.coordinate.latitude
            long = mylocation.coordinate.longitude
        } else {
            print("location not found")
            let alert = UIAlertController(title: nil, message: "Wait until your location found!", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
            return
        }
        var photo_encode = [Any]()
        
        if namePlace.isEmpty || categoryName.isEmpty || description.isEmpty || address.isEmpty || phone.isEmpty ||
        hours.isEmpty || locationName.isEmpty {
            let alert = UIAlertController(title: nil, message: "All fields are required!", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
            return
        }
        if !isPhotos[0] || !isPhotos[2] {
            let alert = UIAlertController(title: nil, message: "Please complete main photos!", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
            return
        }
        photo_encode.removeAll()
        for i in 0 ..< photosBtn.count {
            if isPhotos[i] {
                let image = photosImage[i].image!
                let imageData = image.pngData()
                let base64String = imageData!.base64EncodedString(options: .lineLength64Characters)
                let object = convertJsonString(name: photoName[i], photos: base64String)!
                photo_encode.append(object)
            }
        }
        
        self.postSuggest(id: id, name: namePlace, idCategory: category, idLocation: location, description: description,
                         address: address, phone: phone, hours: hours, url: url, lat: lat, long: long, photos: String(describing: photo_encode))
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "PlaceInfo" {
            let vc = segue.destination as? PlaceFieldInfoVC
            self.placeInfo = vc
        }
        if segue.identifier == "PlaceLocation" {
            let vc = segue.destination as? PlaceFieldLocation
            self.placeLocation = vc
        }
    }
    
    func postSuggest(id: Int, name: String, idCategory: Int, idLocation: Int, description: String, address: String, phone: String,
                     hours: String, url: String, lat: Double, long: Double, photos: String) {
        self.submitBtn.isEnabled = false
        self.activityId.startAnimating()
        let parameters: [String : Any] = [
            "id_member": id,
            "name": name,
            "id_placecategory": idCategory,
            "id_location": idLocation,
            "description": description,
            "address": address,
            "phone": phone,
            "hours": hours,
            "url": url,
            "lat": lat,
            "long": long,
            "photos": photos
        ]
        
        let headers = [
            "User-Agent": "iphone",
            "Referer": "CROCODICHEBAT"
        ]
        
        let url = URL(string: Config().BASE_API_CUSTOM + Config().ADD_SUGGEST_PLACE + Config().SecretKey())!
        Alamofire.request(url, method: .post, parameters: parameters, headers: headers)
            .responseJSON { (response) in
                if response.result.isSuccess {
                    let json = try! JSON(data:response.data!)
                    let message = json["api_message"].stringValue
                    let status = json["api_status"].intValue
                    let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
                    if status == 1 {
                        let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                            for i in 0 ..< self.photosBtn.count {
                                if self.isPhotos[i] {
//                                    self.photosBtn[i].setImage(self.addPhoto, for: .normal)
                                    self.photosImage[i].image = self.addPhoto
                                    self.isPhotos[i] = false
                                }
                            }
                            self.placeInfo.Reset()
                            self.placeLocation.Reset()
                        })
                        alert.addAction(ok)
                    } else {
                        let ok = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                        alert.addAction(ok)
                    }
                    self.present(alert, animated: true, completion: nil)
                } else {
                    
                }
                self.activityId.stopAnimating()
                self.submitBtn.isEnabled = true
                
        }
    }
    
    func convertJsonString(name: String, photos: String) -> String?{
        let para:NSMutableDictionary = NSMutableDictionary()
        para.setValue(name, forKey: "name")
        para.setValue(photos, forKey: "photo_encode")
        // let jsonError: NSError?
        let jsonData: Data
        do{
            jsonData = try JSONSerialization.data(withJSONObject: para, options: .init(rawValue: 0))
            let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            return jsonString
        } catch _ {
            print ("UH OOO")
            return nil
        }
    }
}
