//
//  PlaceFieldLocation.swift
//  HalalWorld
//
//  Created by Crocodic Studio on 4/26/16.
//  Copyright © 2016 Crocodic Studio. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class PlaceFieldLocation: UITableViewController {
    
    var items = [ListLocation]()
    @IBOutlet weak var locationPlace: UIButton!
    @IBOutlet weak var locationIndicator: UIActivityIndicatorView!
    var locationId = Int()
    var location = String()
    let deselectButton = UIColor(red: 204/255, green: 204/255, blue: 204/255, alpha: 1.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.postListLocation()
    }
    
    func Reset() {
        self.locationPlace.setTitle("City", for: .normal)
        self.locationPlace.setTitleColor(deselectButton, for: .normal)
        self.locationId = 0
        self.location = ""
    }
    
    func ShowAlert() {
        self.SelectLocation(categories: items)
    }
    
    func SelectLocation(categories: [ListLocation]) {
        if self.location != "" {
            self.locationPlace.setTitleColor(UIColor.black, for: .normal)
        } else {
            self.locationPlace.setTitleColor(deselectButton, for: .normal)
        }
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        for i in 0 ..< items.count {
            var sheet = UIAlertAction()
            if i == items.count - 1 {
                sheet = UIAlertAction(title: items[i].name, style: .cancel, handler: nil)
            } else {
                sheet = UIAlertAction(title: items[i].name, style: .default) { (action) -> Void in
                    self.locationPlace.setTitle(self.items[i].name, for: .normal)
                    self.locationPlace.setTitleColor(UIColor.black, for: .normal)
                    self.locationId = self.items[i].id
                    self.location = self.items[i].name
                }
                sheet.setValue(0, forKey: "titleTextAlignment")
            }
            actionSheet.addAction(sheet)
        }
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func postListLocation() {
        let headers = [
            "User-Agent": "iphone",
            "Referer": "CROCODICHEBAT"
        ]
        
        let url = URL(string: Config().BASE_URL + Config().LIST_LOCATION + Config().SecretKey())!
        Alamofire.request(url, method: .post, headers: headers)
            .responseJSON { (response) in
                if response.result.isSuccess {
                    let json = try! JSON(data:response.data!)
                    let status = json["api_status"].intValue
                    let totalData = json["api_total_data"].intValue
                    if status == 1 {
                        for i in 0 ..< totalData + 1 {
                            if i == totalData{
                                self.items.append(ListLocation(name: "Cancel"))
                            } else {
                                let id = json["data"][i]["id"].intValue
                                let name = json["data"][i]["name"].stringValue
                                self.items.append(ListLocation(id: id, name: name))
                            }
                        }
                    } else {
                        return self.postListLocation()
                    }
                } else {
                    return self.postListLocation()
                }
                self.locationPlace.isEnabled = true
                self.locationPlace.setTitle("City", for: .normal)
                self.locationIndicator.stopAnimating()
        }
    }
}
