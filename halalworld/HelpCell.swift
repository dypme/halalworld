//
//  HelpCell.swift
//  HalalWorld
//
//  Created by Crocodic Studio on 5/23/16.
//  Copyright © 2016 Crocodic Studio. All rights reserved.
//

import UIKit

class HelpCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
