//
//  TableViewCell.swift
//  halalworld
//
//  Created by Crocodic Studio on 3/30/16.
//  Copyright © 2016 Crocodic Studio. All rights reserved.
//

import UIKit
import Kingfisher
import Alamofire
import SwiftyJSON
import GoogleMaps

protocol TableViewCellDelegate {
    func shareBtn(cell: TableViewCell)
    func favoriteBtn(cell: TableViewCell)
}

class TableViewCell: UITableViewCell {

    var delegate: TableViewCellDelegate?
    
    var title = String()
    var status = String()
    var distance = String()
    var favorite = Bool()
    
    let favoriteOff = UIImage(named: "favorite_icon_off_44.png")
    let favoriteOn = UIImage(named: "favorite_icon_on_44.png")
    
    @IBOutlet weak var photoImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var favoriteView: UIImageView!
    @IBOutlet weak var shareView: UIImageView!
    @IBOutlet weak var favoriteBtn: UIButton!
    @IBOutlet weak var shareBtn: UIButton!
    @IBOutlet weak var photoView: UIView!
    @IBOutlet weak var activityId: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        shareBtn.addTarget(self, action: #selector(share(sender:)), for: .touchUpInside)
        favoriteBtn.addTarget(self, action: #selector(favorite(sender:)), for: .touchUpInside)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configureData(name: String, category: String, distance: String, favorite: Bool) {
        self.title = name
        self.titleLabel.text = self.title
        self.status = category
        self.statusLabel.text = self.status
        self.distance = distance
        self.distanceLabel.text = self.distance
        self.favorite = favorite
        if favorite {
            self.favoriteView.image = favoriteOn
        } else {
            self.favoriteView.image = favoriteOff
        }
    }
    
    @objc func share(sender: AnyObject) {
        delegate?.shareBtn(cell: self)
    }
    
    @objc func favorite(sender: AnyObject) {
        delegate?.favoriteBtn(cell: self)
    }
    
    func getDirection(targetLocation: CLLocationCoordinate2D) {
        var direction = String()
        let targetLat = targetLocation.latitude
        let targetLong = targetLocation.longitude
        
        #if !(targetEnvironment(simulator))
            let myLat  = UserDefaults.standard.double(forKey: "myLocationLat")
            let myLong = UserDefaults.standard.double(forKey: "myLocationLong")
        #else
            let myLat = 35.709026 // Tokyo latitude
            let myLong = 139.731992 // Tokyo longitude
        #endif
        
        let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(targetLat),\(targetLong)&destination=\(myLat),\(myLong)&key\(Config().APIKeyServer)")
        Alamofire.request(url!, method: .get)
            .responseJSON { (response) in
                if response.result.isSuccess {
                    let json = try! JSON(data:response.data!)
                    let status = json["status"].stringValue
                    if status == "OK" {
                        let dir = json["routes"][0]["legs"][0]["distance"]["text"].stringValue
                        direction = dir + " from my location"
                        self.distanceLabel.fadeTransition(duration: 0.4)
                        self.distanceLabel.text = direction
                    }
                } else {
                    
                }
            }.resume()
    }
    
}
