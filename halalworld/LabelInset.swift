//
//  LabelInset.swift
//  halalworld
//
//  Created by Crocodic Studio on 4/6/16.
//  Copyright © 2016 Crocodic Studio. All rights reserved.
//

import UIKit

@IBDesignable
class LabelInset: UILabel {

    @IBInspectable var top: CGFloat = 0
    @IBInspectable var left: CGFloat = 0
    @IBInspectable var bottom: CGFloat = 0
    @IBInspectable var right: CGFloat = 0
    
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets.init(top: self.top, left: self.left, bottom: self.bottom, right: self.right)
        super.drawText(in: rect.inset(by: insets))
    }

}
