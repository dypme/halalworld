//
//  RegisterTableVC.swift
//  HalalWorld
//
//  Created by Crocodic Studio on 4/11/16.
//  Copyright © 2016 Crocodic Studio. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class RegisterTableVC: UITableViewController {

    @IBOutlet weak var emailField: TextFieldInset!
    @IBOutlet weak var passwordField: TextFieldInset!
    @IBOutlet weak var repasswordField: TextFieldInset!
    @IBOutlet weak var register: UIButton!
    
    var activityInd: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func registerBtn(sender: AnyObject) {
        self.view.endEditing(true)
        let email = emailField.text
        let password = passwordField.text
        let repassword = repasswordField.text
        
        if email!.isEmpty || password!.isEmpty || repassword!.isEmpty {
            self.displayALert(message: "All fields are required", success: false)
            return
        }
        
        if password != repassword {
            self.displayALert(message: "Password do not match", success: false)
            return
        }
        
        self.post(email: email!, password: password!)
        
    }
    
    func displayALert(message: String, success: Bool) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        
        var ok = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        
        if success {
            ok = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                self.navigationController?.popViewController(animated: true)
            })
        }
        
        alert.addAction(ok)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func post(email: String, password: String) {
        self.loading(loading: true)
        let parameters = [
            "email": email,
            "password": password
        ]
        
        let headers = [
            "User-Agent": "iphone",
            "Referer": "CROCODICHEBAT"
        ]
        
        let url = URL(string: Config().BASE_URL + Config().REGISTER + Config().SecretKey())!
        Alamofire.request(url, method: .post, parameters: parameters, headers: headers)
            .responseJSON { (response) in
                if response.result.isSuccess {
                    let json = try! JSON(data:response.data!)
                    let status = json["api_status"].intValue
                    self.displayALert(message: "\(json["api_message"].stringValue)", success: true)
                    if status == 1 {
                        self.register.isEnabled = false
                    } else {
                        
                    }
                } else {
                    
                }
                self.loading(loading: false)
            }
    }
    
    func loading(loading: Bool) {
        if loading {
            activityInd.startAnimating()
        } else {
            activityInd.stopAnimating()
        }
    }
}
