//
//  ListPlaceLocation.swift
//  HalalWorld
//
//  Created by Crocodic Studio on 7/26/16.
//  Copyright © 2016 Crocodic Studio. All rights reserved.
//

import Foundation
import MapKit

class ListPlaceLocation {
    var id = Int()
    var favoriteId = Int()
    var photo = String()
    var placeCategory = String()
    var name = String()
    var description = String()
    var direction = String()
    var lat = Double()
    var long = Double()
    var isFavorite = Bool()
    var appstore = String()
    
    init (
        id: Int,
        favoriteId: Int,
        photo: String,
        name: String,
        description: String,
        placeCategory: String,
        lat: Double,
        long: Double,
        isFavorite: Bool,
        appstore: String) {
        self.id = id
        self.favoriteId = favoriteId
        self.photo = photo
        self.placeCategory = placeCategory
        self.name = name
        self.description = description
        self.lat = lat
        self.long = long
        self.isFavorite = isFavorite
        self.appstore = appstore
    }
}
