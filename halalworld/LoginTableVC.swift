//
//  LoginTableViewController.swift
//  halalworld
//
//  Created by Crocodic Studio on 3/29/16.
//  Copyright © 2016 Crocodic Studio. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class LoginTableVC: UITableViewController {

    @IBOutlet weak var emailField: TextFieldInset!
    @IBOutlet weak var passwordField: TextFieldInset!
    
    var activityInd: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func loginBtn(sender: AnyObject) {
        self.view.endEditing(true)
        loading(loading: true)
        let userEmail = emailField.text!
        let userPassword = passwordField.text!
        
        // check here for userEmail and userPassword
        let parameters = [
            "email": userEmail,
            "password": userPassword
        ]
        
        let headers = [
            "User-Agent": "iphone",
            "Referer": "CROCODICHEBAT"
        ]
        
        let url = URL(string: Config().BASE_URL + Config().LOGIN + Config().SecretKey())!
        Alamofire.request(url, method: .post, parameters: parameters, headers: headers)
            .responseJSON { (response) in
                if response.result.isSuccess {
                    let json = try! JSON(data:response.data!)
                
                    let id = json["id"].intValue
                    let name = json["fullname"].stringValue
                    let email = json["email"].stringValue
                    let gender = json["gender"].stringValue
                    let nationality = json["nationality"].stringValue
                    let age = json["age"].intValue
                    let address = json["address"].stringValue
                    let phone = json["phone"].stringValue
                
                    let status = json["api_status"].intValue
                    self.dialogAlert(message: "\(json["api_message"].stringValue)", status: status)
                    if status == 1 {
                        UserDefaults.standard.set(true, forKey: "isUserLoggedIn")
                        UserDefaults.standard.set(id, forKey: "userId")
                        UserDefaults.standard.set(name, forKey: "userName")
                        UserDefaults.standard.set(email, forKey: "userEmail")
                        UserDefaults.standard.set(gender, forKey: "userGender")
                        UserDefaults.standard.set(nationality, forKey: "userNationality")
                        UserDefaults.standard.set(age, forKey: "userAge")
                        UserDefaults.standard.set(address, forKey: "userAddress")
                        UserDefaults.standard.set(userPassword, forKey: "userPassword")
                        UserDefaults.standard.set(phone, forKey: "userPhone")
                    } else {
                    
                    }
                } else {
                    
                }
                self.loading(loading: false)
        }
    }
    
    func dialogAlert(message: String, status: Int) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        
        var ok = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        if status == 1 {
            ok = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                self.navigationController?.popViewController(animated: true)
            })
        }
        
        alert.addAction(ok)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func loading(loading: Bool) {
        if loading {
            activityInd.startAnimating()
        } else {
            activityInd.stopAnimating()
        }
    }
}
