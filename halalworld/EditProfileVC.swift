//
//  EditProfileVC.swift
//  HalalWorld
//
//  Created by Crocodic Studio on 4/7/16.
//  Copyright © 2016 Crocodic Studio. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class EditProfileVC: UIViewController {

    @IBOutlet weak var saveBtn: UIButton!
    
    private var editProfileEmbed: EditProfileTableVC!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        
        self.navigationController?.navigationItem.title = "Edit Profile"
        
        saveBtn.backgroundColor = UIColor.clear
        saveBtn.layer.cornerRadius = 20
        saveBtn.layer.borderWidth = 1
        saveBtn.layer.borderColor = UIColor(red: 63/255, green: 131/255, blue: 178/255, alpha: 1).cgColor
        
        saveBtn.addTarget(self, action: #selector(updateProfile(sender:)), for: .touchUpInside)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "editProfileEmbed" {
            let vc = segue.destination as? EditProfileTableVC
            self.editProfileEmbed = vc
        }
    }

    @objc func updateProfile(sender: AnyObject) {
        self.view.endEditing(true)
        let id = UserDefaults.standard.integer(forKey:"userId")
        let username = editProfileEmbed.username.text!
        let phoneCode = editProfileEmbed.phoneCode
        let phone = editProfileEmbed.phone.text!
        let gender = editProfileEmbed.genderSelected
        let nationality = editProfileEmbed.nationality
        let age = editProfileEmbed.age.text!
        let address = editProfileEmbed.address.text!
        
        if username.isEmpty || (phone.isEmpty && phoneCode.isEmpty) || address.isEmpty || age.isEmpty || gender.isEmpty || nationality.isEmpty {
            self.dialogAlert(message: "Please fill required form!", status: 0)
            return
        }
        
        if (!phone.isEmpty && phoneCode.isEmpty) || (phone.isEmpty && !phoneCode.isEmpty) {
            self.dialogAlert(message: "Please complete phone number!", status: 0)
            return
        }
        
        // check here for userEmail and userPassword
        let parameters = [
            "id": id,
            "fullname": username,
            "phone": phoneCode + phone,
            "gender": gender,
            "nationality": nationality,
            "age": age,
            "address": address
            ] as [String : Any]
        
        let headers = [
            "User-Agent": "iphone",
            "Referer": "CROCODICHEBAT"
        ]
        
        let url = URL(string: Config().BASE_URL + Config().UPDATE_MEMBER + Config().SecretKey())!
        Alamofire.request(url, method: .post, parameters: parameters, headers: headers)
            .responseJSON { (response) in
                if response.result.isSuccess {
                    let json = try! JSON(data:response.data!)
                    let status = json["api_status"].intValue
                    self.dialogAlert(message: "\(json["api_message"].stringValue)", status: status)
                    if status == 1 {
                        UserDefaults.standard.set(username, forKey: "userName")
                        UserDefaults.standard.set(gender, forKey: "userGender")
                        UserDefaults.standard.set(nationality, forKey: "userNationality")
                        UserDefaults.standard.set(Int(age)!, forKey: "userAge")
                        UserDefaults.standard.set(address, forKey: "userAddress")
                        UserDefaults.standard.set(phoneCode + phone, forKey: "userPhone")
                    } else {
                        
                    }
                } else {
                    
                }
        }
    }
    
    func dialogAlert(message: String, status: Int) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        
        var ok = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        if status == 1 {
            ok = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                self.navigationController?.popViewController(animated: true)
            })
        }
        
        alert.addAction(ok)
        
        self.present(alert, animated: true, completion: nil)
    }

}
