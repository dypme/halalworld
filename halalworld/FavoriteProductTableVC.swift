//
//  FavoriteProductTableVC.swift
//  halalworld
//
//  Created by Crocodic Studio on 4/5/16.
//  Copyright © 2016 Crocodic Studio. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import Alamofire
import SwiftyJSON
import Kingfisher
import GoogleMaps

class FavoriteProductTableVC: UITableViewController, IndicatorInfoProvider, GMSMapViewDelegate, TableViewCellDelegate {

    let cellIdentifier = "postCell"
    var itemInfo = IndicatorInfo(title: "View")
    
    var totalData = Int()
    
    var label = UILabel()
    var activityId = UIActivityIndicatorView()
    
    init(style: UITableView.Style, itemInfo: IndicatorInfo, activityId: UIActivityIndicatorView) {
        self.itemInfo = itemInfo
        self.activityId = activityId
        super.init(style: style)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var datas = [DataList]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "TableViewCell", bundle: Bundle.main), forCellReuseIdentifier: cellIdentifier)
        tableView.estimatedRowHeight = 115
        tableView.rowHeight = UITableView.automaticDimension
        label.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
        label.text = "Data Not Found"
        label.textAlignment = .center
        label.center = self.view.center
        label.isHidden = true
        view.addSubview(label)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let isUserLoggedIn = UserDefaults.standard.bool(forKey: "isUserLoggedIn")
        if isUserLoggedIn {
            self.postList()
        } else {
            self.tableView.separatorStyle = .none
            self.label.isHidden = false
        }
    }
    
    override func viewDidLayoutSubviews() {
        label.center.y = self.view.center.y
    }
    
    func postList() {
        if datas.count == 0 {
            activityId.startAnimating()
            label.isHidden = !activityId.isHidden
        }
        let userId = UserDefaults.standard.integer(forKey:"userId")
        
        let parameters = [
            "id_member": userId
        ]
        
        let headers = [
            "User-Agent": "iphone",
            "Referer": "CROCODICHEBAT"
        ]
        
        let url = URL(string: Config().BASE_API_CUSTOM + Config().LIST_PRODUCT_FAVORITE + Config().SecretKey())!
        Alamofire.request(url, method: .post,parameters: parameters, headers: headers)
            .responseJSON { (response) in
                if response.result.isSuccess {
                    let json = try! JSON(data:response.data!)
                    let status = json["api_status"].intValue
                    if status == 1 {
                        self.datas.removeAll()
                        self.tableView.reloadData()
                        let dataTotal = json["api_total_data"].intValue
                        self.totalData = dataTotal
                        if dataTotal > 0{
                            self.label.isHidden = true
                        } else {
                            self.label.isHidden = false
                        }
                        for i in 0 ..< dataTotal {
                            let favoriteId = json["data"][i]["id"].intValue
                            let idProduct = json["data"][i]["product_id"].intValue
                            let name = json["data"][i]["name"].stringValue
                            let description = json["data"][i]["product_description"].stringValue
                            let placeCategory = json["data"][i]["category"].stringValue
                            let photo = json["data"][i]["url_photo"].stringValue
                            let lat = json["data"][i]["lat"].doubleValue
                            let long = json["data"][i]["long"].doubleValue
                            let appstore = json["data"][i]["appstore"].stringValue
                            let coordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
                            self.datas.append(
                                DataList(
                                    id: idProduct,
                                    favoriteId: favoriteId,
                                    name: name,
                                    description: description,
                                    category: placeCategory,
                                    photo: photo,
                                    direction: "",
                                    favorite: true,
                                    appstore: appstore,
                                    coordinate: coordinate
                                ))
                        }
                        self.tableView.separatorStyle = .singleLine
                    } else {
                        self.datas.removeAll()
                        self.label.isHidden = false
                        self.tableView.separatorStyle = .none
                    }
                } else {
                    
                }
                self.activityId.stopAnimating()
                if self.datas.isEmpty {
                    self.label.isHidden = !self.activityId.isHidden
                }
                self.doRefresh()
        }
    }
    
    func doRefresh(){
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func shareBtn(cell: TableViewCell) {
        let indexPath = tableView.indexPathForRow(at: cell.center)!
        let placeName = self.datas[indexPath.row].name
        let placeDescription = self.datas[indexPath.row].description.isEmpty ? "-" : self.datas[indexPath.row].description.trunc(length: 30)
        let shareUrl = self.datas[indexPath.row].appstore
        let photo = self.datas[indexPath.row].photo
        
        let url = URL(string: photo)!
        ImageCache.default.retrieveImage(forKey: photo, options: nil, completionHandler: { (image, cacheType) in
            if let image = image {
                let objectsToShare = [image, placeName, placeDescription, shareUrl] as [Any]
                let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                self.present(activityVC, animated: true, completion: nil)
            } else {
                ImageDownloader.default.downloadImage(with: url, progressBlock: nil, completionHandler: { (image, error, imageURL, originalData) in
                    if error == nil {
                        ImageCache.default.store(image!, forKey: photo)
                        let objectsToShare = [image!, placeName, placeDescription, shareUrl] as [Any]
                        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                        self.present(activityVC, animated: true, completion: nil)
                    }
                })
            }
        })
    }
    
    func favoriteBtn(cell: TableViewCell) {
        let indexPath = tableView.indexPathForRow(at: cell.center)!
        let isUserLoggedIn = UserDefaults.standard.bool(forKey: "isUserLoggedIn")
        if !isUserLoggedIn {
            return
        }
        if datas[indexPath.row].favorite {
            removeFavorite(cell: cell, indexPath: indexPath)
        }
    }
    
    func removeFavorite(cell: TableViewCell, indexPath: IndexPath) {
        cell.favoriteBtn.isEnabled = false
        let id = datas[indexPath.row].favoriteId
        let headers = [
            "User-Agent": "iphone",
            "Referer": "CROCODICHEBAT"
        ]
        
        let url = URL(string: Config().BASE_URL + Config().DELETE_FAVORITE + Config().SecretKey())!
        Alamofire.request(url, method: .post, parameters: ["id": id], headers: headers)
            .responseJSON { (response) in
                if response.result.isSuccess {
                    let json = try! JSON(data:response.data!)
                    let status = json["api_status"].intValue
                    if status == 1 {
                        self.datas.remove(at: indexPath.row)
                        self.tableView.deleteRows(at: [indexPath], with: .top)
                        if self.datas.isEmpty {
                            self.label.isHidden = false
                        } else {
                            self.label.isHidden = true
                        }
                    } else {
                        
                    }
                } else {
                    
                }
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datas.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! TableViewCell
        cell.delegate = self
        let photo = datas[indexPath.row].photo
        let name = datas[indexPath.row].name
        let direction = datas[indexPath.row].direction
        let category = datas[indexPath.row].category
        let favorite = datas[indexPath.row].favorite
        let coordinate = datas[indexPath.row].coordinate
        
        let url = URL(string: photo)!
        cell.photoImage.kf.setImage(with: url) { (image, error, cacheType, imageURL) in
            cell.photoImage.image = cell.photoImage.image?.circle
            cell.activityId.stopAnimating()
        }

        cell.configureData(name: name, category: category, distance: direction, favorite: favorite)
        cell.getDirection(targetLocation: coordinate)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let favoriteId = datas[indexPath.row].favoriteId
        let productId = datas[indexPath.row].id
        let isFavorite = datas[indexPath.row].favorite
        
        UserDefaults.standard.set(favoriteId, forKey: "favoriteProductId")
        UserDefaults.standard.set(productId, forKey: "productId")
        UserDefaults.standard.set(isFavorite, forKey: "isFavorite")
        
        let mainStoryBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc:UIViewController = mainStoryBoard.instantiateViewController(withIdentifier: "DetailProduct")
        self.present(vc, animated: true, completion: nil)
    }
    
    // MARK: - IndicatorInfoProvider
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }

}
