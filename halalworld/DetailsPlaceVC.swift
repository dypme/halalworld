//
//  DetailsPlaceVC.swift
//  HalalWorld
//
//  Created by Crocodic Studio on 4/19/16.
//  Copyright © 2016 Crocodic Studio. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Kingfisher

class DetailsPlaceVC: UIViewController {
    
    @IBOutlet weak var mainPhoto: UIImageView!
    @IBOutlet weak var placeName: UILabel!
    @IBOutlet weak var placeCategory: UILabel!
    @IBOutlet weak var placeDescription: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var phone: UILabel!
    @IBOutlet weak var hours: UILabel!
    @IBOutlet weak var url: UILabel!
    @IBOutlet weak var activityId: UIActivityIndicatorView!
    @IBOutlet weak var imageId: UIActivityIndicatorView!
    @IBOutlet weak var favorite: UIButton!
    
    @IBOutlet var listPhotoindicator: Array<UIActivityIndicatorView>!
    @IBOutlet var listPhotoView: Array<UIImageView>!
    
    var listPhoto = [UIImage]()
    var indexPhoto = 0
    
    var shareUrl: URL!
    
    var isFavorite = Bool()
    let favoriteOff = UIImage(named: "favorite_icon_off_44.png")
    let favoriteOn = UIImage(named: "favorite_icon_on_44.png")
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.postData()
        self.photoPlace()
        self.favorite.isEnabled = false
        self.favorite.setImage(self.favoriteOff, for: .normal)
        isFavorite = UserDefaults.standard.bool(forKey: "isFavorite")
    }
    
    func postData() {
        let placeId = UserDefaults.standard.integer(forKey:"placeId")
        
        let headers = [
            "User-Agent": "iphone",
            "Referer": "CROCODICHEBAT"
        ]
        
        let url = URL(string: Config().BASE_URL + Config().DETAIL_PLACE + Config().SecretKey())!
        Alamofire.request(url, method: .post, parameters: ["id": placeId], headers: headers)
            .responseJSON { (response) in
                if response.result.isSuccess {
                    let json = try! JSON(data:response.data!)
                    let status = json["api_status"].intValue
                    if status == 1 {
                        let name = json["name"].stringValue
                        let category = json["placecategory_name"].stringValue
                        let phone = json["phone"].intValue == 0 ? 0: json["phone"].intValue
                        let hours = json["hours"].intValue == 0 ? 0: json["hours"].intValue
                        let url = json["url"].stringValue == "" ? "-": json["url"].stringValue
                        let description = json["description"].stringValue == "" ? "-": json["description"].stringValue
                        let address = json["address"].stringValue == "" ? "-": json["address"].stringValue
                        let photo = json["url_photo"].stringValue
                        let urlShare = json["appstore"].stringValue
                    
                        self.placeName.text = name
                        self.placeCategory.text = category
                        self.placeDescription.text = description
                        self.phone.text = String(phone)
                        self.hours.text = String(hours)
                        self.url.text = url
                        self.address.text = address
                        self.shareUrl = URL(string: urlShare)!
                        self.mainImage(url: photo)
                    
                        self.activityId.stopAnimating()
                        if self.isFavorite {
                            self.favorite.setImage(self.favoriteOn, for: .normal)
                        }
                        self.favorite.isEnabled = true
                    } else {
                        return self.postData()
                    }
                } else {
                    return self.postData()
                }
                
        }
    }
    
    func photoPlace() {
        let placeId = UserDefaults.standard.integer(forKey:"placeId")
        
        let headers = [
            "User-Agent": "iphone",
            "Referer": "CROCODICHEBAT"
        ]
        
        let url = URL(string: Config().BASE_URL + Config().LIST_PHOTO_PLACE + Config().SecretKey())!
        Alamofire.request(url, method: .post, parameters: ["id_place": placeId], headers: headers)
            .responseJSON { (response) in
                if response.result.isSuccess {
                    let json = try! JSON(data:response.data!)
                    let status = json["api_status"].intValue
                    if status == 1 {
                        let totalData = json["api_total_data"].intValue
                        for i in 0 ..< totalData {
                            self.listPhoto.append(UIImage())
                            self.listPhotoindicator[i].startAnimating()
                            let url = json["data"][i]["url_photo"].stringValue
                            self.image(index: i, url: url)
                        }
                    } else {
                        return self.photoPlace()
                    }
                } else {
                    return self.photoPlace()
                }
        }
        
    }
    
    func mainImage(url: String) {
        let url = URL(string: url)!
        self.mainPhoto.kf.setImage(with: url) { (image, error, cacheType, imageURL) in
            self.mainPhoto.layer.masksToBounds = true
            self.imageId.stopAnimating()
        }
    }
    
    func image(index: Int, url:String) {
        let newUrl = URL(string: url)!
        let newIndex = self.listPhoto.count - 1 - index
        ImageCache.default.retrieveImage(forKey: url, options: nil) { (image, cacheType) in
            if let image = image {
                self.listPhoto[newIndex] = image
                self.listPhotoView[newIndex].image = self.listPhoto[newIndex].circle
                self.listPhotoindicator[index].stopAnimating()
            } else {
                ImageDownloader.default.downloadImage(with: newUrl, progressBlock: nil, completionHandler: { (image, error, imageURL, originalData) in
                    if error == nil {
                        ImageCache.default.store(image!, forKey: url)
                        self.listPhoto[newIndex] = image!
                        self.listPhotoView[newIndex].image = self.listPhoto[newIndex].circle
                        self.listPhotoindicator[index].stopAnimating()
                    }
                })
            }
        }
    }
    
    @IBAction func backBtn(sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
        UserDefaults.standard.removeObject(forKey: "placeId")
        UserDefaults.standard.removeObject(forKey: "isFavorite")
    }
    
    @IBAction func nextImage(sender: AnyObject) {
        if indexPhoto < listPhoto.count - 1 {
            indexPhoto += 1
            self.mainPhoto.image = listPhoto[indexPhoto]
        }
    }
    
    @IBAction func prevImage(sender: AnyObject) {
        if indexPhoto > 0 {
            indexPhoto -= 1
            self.mainPhoto.image = listPhoto[indexPhoto]
        }
    }
    
    @IBAction func shareBtn(sender: AnyObject) {
        if !self.placeName.text!.isEmpty && !self.placeDescription.text!.isEmpty {
            let placeName = self.placeName.text!
            let placeDescription = self.placeDescription.text!.trunc(length: 30)
            let objectsToShare = [(self.mainPhoto?.image)!, placeName, placeDescription, self.shareUrl.absoluteString] as [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            self.present(activityVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func favoriteBtn(sender: AnyObject) {
        let isUserLoggedIn = UserDefaults.standard.bool(forKey: "isUserLoggedIn")
        
        if !isUserLoggedIn {
            let alert = UIAlertController(title: nil, message: "Please login first!", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alert.addAction(ok)
            self.present(alert, animated: true, completion: nil)
            return
        }
        self.favorite.isEnabled = false
        if self.isFavorite {
            self.favorite.setImage(self.favoriteOff, for: .normal)
            self.isFavorite = false
            self.removeFavorite()
            return
        }
        
        self.favorite.setImage(self.favoriteOn, for: .normal)
        self.isFavorite = true
        self.addFavorite()
    }
    
    func removeFavorite() {
        let id = UserDefaults.standard.integer(forKey:"favoritePlaceId")
        
        let headers = [
            "User-Agent": "iphone",
            "Referer": "CROCODICHEBAT"
        ]
        
        let url = URL(string: Config().BASE_URL + Config().DELETE_FAVORITE + Config().SecretKey())!
        Alamofire.request(url, method: .post, parameters: ["id": id], headers: headers)
            .responseJSON { (response) in
                if response.result.isSuccess {
                    let json = try! JSON(data:response.data!)
                    let status = json["api_status"].intValue
                    if status == 1 {
                        
                    } else {
                    
                    }
                } else {
                
                }
                self.favorite.isEnabled = true
        }
    }
    
    func addFavorite() {
        let placeId = UserDefaults.standard.integer(forKey:"placeId")
        let idMember = UserDefaults.standard.integer(forKey:"userId")
        
        let parameters = [
            "id_member": idMember,
            "placepro": "place",
            "id_place": placeId
            ] as [String : Any]
        let headers = [
            "User-Agent": "iphone",
            "Referer": "CROCODICHEBAT"
        ]
        
        let url = URL(string: Config().BASE_URL + Config().ADD_FAVORITE_PLACE + Config().SecretKey())!
        Alamofire.request(url, method: .post, parameters: parameters, headers: headers)
            .responseJSON { (response) in
                if response.result.isSuccess {
                    let json = try! JSON(data:response.data!)
                    let status = json["api_status"].intValue
                    if status == 1 {
                        
                    } else {
                    
                    }
                } else {
                    
                }
                self.favorite.isEnabled = true
        }
    }

}
